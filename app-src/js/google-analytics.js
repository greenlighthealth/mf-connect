var CONSTANTS = require('./constants');

var googleAnalytics = (function () {
    'use strict';

    var googleAnalyticslogging;
    var GA_DIMENSION = CONSTANTS.GA_DIMENSION;
    var GA_EVENT = CONSTANTS.GA_EVENT;

    return {

        setGAConfigs: function (options) {
            if (options.enableGoogleAnalyticsTracking) {
                var GA_TRACKING_ID_DEV = 'UA-160990104-4';
                var GA_TRACKING_ID_PROD = 'UA-160990104-1';
                var analyticsURL;
                googleAnalyticslogging = true;

                var GA_TOKEN = GA_TRACKING_ID_PROD;
                
                if (options.gaHDCToken) {
                    GA_TOKEN = options.gaHDCToken;
                } else if (this.isDevelopment() && !window.location.host.includes('prod')) {
                    GA_TOKEN = GA_TRACKING_ID_DEV;
                }

                if (options.enableGoogleAnlayticsDebug) {
                    analyticsURL = 'https://www.google-analytics.com/analytics_debug.js';
                } else {
                    analyticsURL = 'https://www.google-analytics.com/analytics.js';
                }

                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script', analyticsURL,'ga');
    
                ga('create', GA_TOKEN, 'auto');
                ga('set', 'anonymizeIp', true);

                if (options.enableGoogleAnlayticsDebug && location.hostname == 'localhost') {
                    ga('set', 'sendHitTask', null); //prevents hits being sent to GA during development
                }

                this.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, {pageview: location.pathname + location.search}, {customerUuid: options.customerUuid});
            }
        },

        isDevelopment: function () {
            var devEnvironments = ['local', 'dev', 'demo', 'sandbox'];
            var getWindowLocationHost = function checkWindowLocationHost(env) {
                return window.location.host.includes(env);
            };

            return devEnvironments.some(getWindowLocationHost);
        },

        generateGATrackingEventObj: function (eventCategory, eventAction) {
            var eventObj = new Object();
            eventObj.eventCategory = eventCategory;
            eventObj.eventAction = eventAction;
    
            return eventObj;
        },

        generateGAMetadataTracking: function (metadata) {
            for (var property in metadata) {
                switch (property) {
                    case 'connection':
                        ga('set', GA_DIMENSION.CREATE_CONNECTION_ID, metadata[property].id.toString());
                        ga('set', GA_DIMENSION.CREATE_CONNECTION_PORTAL_NAME, metadata[property].portal.name);
                        ga('set', GA_DIMENSION.CREATE_CONNECTION_PORTAL_TYPE, metadata[property].portal.type);
                        ga('set', GA_DIMENSION.CREATE_CONNECTION_PORTAL_PRIMARYURL, metadata[property].portal.primaryUrl);
                        break;
                    case 'connectionId':
                        ga('set', GA_DIMENSION.CONNECTION_ID, metadata.connectionId);
                        break;
                    case 'connectionStatus':
                        ga('set', GA_DIMENSION.CONNECTION_STATUS, metadata.connectionStatus);
                        break;
                    case 'portalId':
                        ga('set', GA_DIMENSION.PORTAL_ID, metadata.portalId);
                        break;
                    case 'term':
                        ga('set', GA_DIMENSION.TERM, metadata.term);
                        break;
                    case 'zipCode':
                        ga('set', GA_DIMENSION.ZIPCODE, metadata.zipCode);
                        break;
                    case 'url':
                        ga('set', GA_DIMENSION.URL, metadata.url);
                        break;
                    case 'error':
                        ga('set', GA_DIMENSION.ERROR, metadata.error);
                        break;
                    case 'resultDisplayName':
                        ga('set', GA_DIMENSION.RESULT_DISPLAY_NAME, metadata.resultDisplayName);
                        break;
                    case 'resultAddress':
                        ga('set', GA_DIMENSION.RESULT_ADDRESS, metadata.resultAddress);
                        break;
                    case 'resultId':
                        ga('set', GA_DIMENSION.RESULT_ID, metadata.resultId);
                        break;
                    case 'externalUrl':
                        ga('set', GA_DIMENSION.EXTERNAL_URL, metadata.externalUrl);
                        break;
                    case 'loadTime':
                        ga('set', GA_DIMENSION.PAGE_LOAD_SPEED, metadata.loadTime);
                        break;
                    case 'customerUuid':
                        ga('set', GA_DIMENSION.CUSTOMER_UUID, metadata.customerUuid);
                        break;
                    default:
                        null;
                }
            }
        },

        trackGAnalyticsEvent: function (gaEventType, trackingData, metadata) {
            if (googleAnalyticslogging) {
                if (gaEventType === GA_EVENT.EVENT) {
                    ga('send', gaEventType, trackingData.eventCategory, trackingData.eventAction, 'MF Connect');
        
                    if (metadata) {
                        this.generateGAMetadataTracking(metadata);
                    }
                } else {
                    ga('send', gaEventType, trackingData.pageview);
                    
                    if (metadata) {
                        this.generateGAMetadataTracking(metadata);
                    }
                }
            }
        },

        startGASiteSpeedTimer: function () { 
            var initialTime = Date.now();
            return initialTime  
        },

        stopGASiteSpeedTimer: function (initialTime) { 
            return ((Date.now() - initialTime)/1000).toString();
        },
    };
})();

module.exports = googleAnalytics;