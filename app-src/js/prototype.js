var mfConnectService = require('./mf-connect-service.js');
var googleAnalytics = require('./google-analytics.js');
var mfUtils = require('./mf-utils.js');
var CONSTANTS = require('./constants');

var prototype = (function () {
    'use strict';

    var GA_EVENT = CONSTANTS.GA_EVENT;
    var USER_EVENTS = CONSTANTS.USER_EVENTS;
    var SEARCH_OPTIONS = CONSTANTS.SEARCH_OPTIONS;

    return {
        invokeEventHandler: function (eventName, eventData, metadata) {
            googleAnalytics.trackGAnalyticsEvent(GA_EVENT.EVENT, eventData, metadata);
            if ("function" === typeof mfConnectService.getMfConnectData()["onEvent"]) {
                console.log('eventData', eventData);
                eventData = eventData || {};
                eventData.target = this.api;
                mfConnectService.getMfConnectData()["onEvent"](eventName, eventData);
            }
        },
        invokeOnCancel: function () {
        this.invokeEventHandler(USER_EVENTS.ON_CANCEL, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_CANCEL));
        },
        invokeOnUserClickReviewConnections: function () {
            this.invokeEventHandler(USER_EVENTS.ON_USER_CLICK_REVIEW_CONNECTIONS, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_USER_CLICK_REVIEW_CONNECTIONS));
        },
        invokeOnUserClickAddConnection: function () {
            this.invokeEventHandler(USER_EVENTS.ON_USER_CLICK_ADD_CONNECTION, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_USER_CLICK_ADD_CONNECTION));
        },
        invokeValidatingCredentialsSuccessEvent: function (connection) {
            this.invokeEventHandler(USER_EVENTS.ON_VALIDATING_CREDENTIALS_SUCCESS, {}, { "connection": connection });
        },
        invokeValidatingCredentialsErrorEvent: function (connection) {
            this.invokeEventHandler(USER_EVENTS.ON_VALIDATING_CREDENTIALS_ERROR, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_VALIDATING_CREDENTIALS_ERROR), { "connection": connection });
        },
        invokeValidatingCredentialsTimeoutEvent: function (connection) {
            this.invokeEventHandler(USER_EVENTS.ON_VALIDATING_CREDENTIALS_TIMEOUT, {}, { "connection": connection });
        },
        invokeOnSearchPortalHandler: function (primaryUrl) {
            var metadata = {"url": primaryUrl};
            this.invokeEventHandler(USER_EVENTS.ON_SEARCH_PORTAL, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_SEARCH_PORTAL), metadata);
        },
        invokeOnRedirectHandler: function (externalUrl) {
            var metadata = { 'externalUrl': externalUrl };
            this.invokeEventHandler(USER_EVENTS.ON_REDIRECT_USER, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_REDIRECT_USER), metadata);
        },
        invokeExistingConnectionEvent: function (eventType, eventData, connection) {
            var metadata = {"connectionId": connection.id};
            metadata.portalId = connection.portal.id;
            metadata.portalType = connection.getPortalType();
            metadata.portalName = connection.portal.name;
    
            this.invokeEventHandler(eventType, eventData, metadata);
        },
        invokeConnectionDeleteEvent: function (connection) {
            this.invokeExistingConnectionEvent(USER_EVENTS.ON_DELETE_CONNECTION, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_DELETE_CONNECTION), connection);
        },
        invokeConnectionUpdateEvent: function (connection) {
            this.invokeExistingConnectionEvent(USER_EVENTS.ON_UPDATE_CONNECTION, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_UPDATE_CONNECTION), connection);
        },
        invokeConnectionRefreshEvent: function (connection) {
            this.invokeExistingConnectionEvent(USER_EVENTS.ON_REFRESH_CONNECTION, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_REFRESH_CONNECTION), connection);
        },
        invokeOnErrorHandler: function (error) {
            var metadata = {"error": error};
            this.invokeEventHandler(USER_EVENTS.ON_ERROR, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_ERROR), metadata);
        },
        invokeOnSelectPortal: function (portal, providerType) {
            var locationHasPortal = undefined !== portal;
            var metadata = {};
            var eventName = USER_EVENTS.ON_SELECT_PORTAL;
            var eventData = googleAnalytics.generateGATrackingEventObj('Modal Event', eventName);

            if (providerType === SEARCH_OPTIONS.DOCTOR) {
                eventName = USER_EVENTS.ON_SELECT_DOCTOR_PORTAL;
            } else if (providerType === SEARCH_OPTIONS.OFFICE) {
                eventName = USER_EVENTS.ON_SELECT_LOCATION_PORTAL;
            }

            if (!locationHasPortal) {
                metadata = {
                    "type": "NO_PORTAL",
                    "message": "The selected location is not associated to any portals yet."
                };
                this.invokeEventHandler(eventName, eventData, metadata);
                return;
            }

            var portalEx = mfUtils.getExtendedPortal(portal);
            if (portalEx.isUnderDevelopment() || portalEx.isPendingPortal()) {
                metadata = {
                    "type": "PLACEHOLDER_PORTAL",
                    "message": "The selected location is currently set to a placeholder portal until a formal mapping has been made."
                };
                this.invokeEventHandler(eventName, eventData, metadata);
                return;
            }

            metadata = {
                "id": portal.id,
                "name": portal.name,
                "type": portal.typeInfo.name
            };
            this.invokeEventHandler(eventName, eventData, metadata);
        },
        invokeOnSearchProviderHandler: function (searchInfo, providerType) {
            var metadata = {"term": searchInfo.searchTerm, "zipCode": searchInfo.zipCode};
            var eventData = {};

            if (providerType === SEARCH_OPTIONS.DOCTOR) {
                eventData = googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_SEARCH_DOCTOR);

                this.invokeEventHandler(USER_EVENTS.ON_SEARCH_DOCTOR, eventData, metadata);
            } else if (providerType === SEARCH_OPTIONS.OFFICE) {
                eventData = googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_SEARCH_LOCATION);

                this.invokeEventHandler(USER_EVENTS.ON_SEARCH_LOCATION, eventData, metadata);
            }
        },
        invokeOnCreateConnectionHandler: function (connection) {
            var metadata = {"connection": connection};
            var eventData = googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_CREATE_CONNECTION);
            this.invokeEventHandler(USER_EVENTS.ON_CREATE_CONNECTION, eventData, metadata);
        },
        invokeOnSelectProviderHandler: function (resultDisplayName, selectedItem, providerType, originalFilter) {
            var eventName = providerType === SEARCH_OPTIONS.DOCTOR ?
            USER_EVENTS.ON_SELECT_DOCTOR :
                originalFilter === SEARCH_OPTIONS.DOCTOR ?
                    USER_EVENTS.ON_SELECT_DOCTOR_LOCATION :
                    USER_EVENTS.ON_SELECT_LOCATION;
            var metadata = {
                'resultDisplayName': resultDisplayName
            };
            var eventData = googleAnalytics.generateGATrackingEventObj('Modal Event', eventName);
            metadata['resultAddress'] = mfUtils.getAddressDisplayName(selectedItem);
            if (selectedItem.providerId) {
                metadata['resultAddress'] = mfUtils.getProviderDisplayAddress(selectedItem);
                metadata['resultId'] = selectedItem.providerId;
            } else if (selectedItem.practiceId) {
                metadata['resultId'] = selectedItem.practiceId;
                metadata['resultAddress'] = mfUtils.getPracticeDisplayAddress(selectedItem);
            } else if (selectedItem.officeId) {
                metadata['resultId'] = selectedItem.officeId;
            } else if (selectedItem.facilityId) {
                metadata['resultId'] = selectedItem.facilityId;
            }
            this.invokeEventHandler(eventName, eventData, metadata);
        },
        hasPreSelectedPortal: function () {
            var portal = mfConnectService.getMfConnectData()["preSelectedPortal"];
            if (portal === undefined) {
                return false;
            }
            if (typeof portal !== "object") {
                return false;
            }
            return portal.hasOwnProperty("portalId");
        }
    };
})();

module.exports = prototype;