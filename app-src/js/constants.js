// Google Analytics constants - GA prefix
var GA_DIMENSION = {
    CONNECTION_ID: 'dimension1',
    CONNECTION_STATUS: 'dimension2',
    PORTAL_ID: 'dimension3',
    TERM: 'dimension4',
    ZIPCODE: 'dimension5',
    URL: 'dimension6',
    ERROR: 'dimension7',
    RESULT_DISPLAY_NAME: 'dimension8',
    RESULT_ADDRESS: 'dimension9',
    RESULT_ID: 'dimension10',
    EXTERNAL_URL: 'dimension11',
    CREATE_CONNECTION_ID: 'dimension12',
    CREATE_CONNECTION_PORTAL_NAME: 'dimension13',
    CREATE_CONNECTION_PORTAL_TYPE: 'dimension14',
    CREATE_CONNECTION_PORTAL_PRIMARYURL: 'dimension15',
    PAGE_LOAD_SPEED: 'dimension16',
    CUSTOMER_UUID: 'dimension17',
}

var GA_EVENT = {
    EVENT: 'event',
    PAGEVIEW: 'pageview'
}

// User Event constants
var USER_EVENTS = {
    "ON_ERROR": "onError",
    "ON_OPEN_DIALOG": "onOpenDialog",
    "ON_CLOSE_DIALOG": "onCloseDialog",
    "ON_SEARCH_DOCTOR": "onSearchDoctor",
    "ON_SELECT_DOCTOR": "onSelectDoctor",
    "ON_SEARCH_LOCATION": "onSearchLocation",
    "ON_SELECT_LOCATION": "onSelectLocation",
    "ON_SELECT_DOCTOR_LOCATION": "onSelectDoctorLocation",
    "ON_SEARCH_PORTAL": "onSearchPortal",
    "ON_SELECT_PORTAL": "onSelectPortal",
    "ON_SELECT_DOCTOR_PORTAL": "onSelectDoctorPortal",
    "ON_SELECT_LOCATION_PORTAL": "onSelectLocationPortal",
    "ON_CREATE_CONNECTION": "onCreateConnection",
    "ON_UPDATE_CONNECTION": "onUpdateConnection",
    "ON_REFRESH_CONNECTION": "onRefreshConnection",
    "ON_DELETE_CONNECTION": "onDeleteConnection",
    "ON_REDIRECT_USER": "onRedirectUser",
    "ON_DONE_MAKING_CONNECTIONS": "onDoneMakingConnections",
    "ON_VALIDATING_CREDENTIALS_ERROR": "onValidatingCredentialsError",
    "ON_VALIDATING_CREDENTIALS_TIMEOUT": "onValidatingCredentialsTimeout",
    "ON_VALIDATING_CREDENTIALS_SUCCESS": "onValidatingCredentialsSuccess",
    "ON_USER_CLICK_REVIEW_CONNECTIONS": "onUserClickReviewConnections",
    "ON_USER_CLICK_ADD_CONNECTION": "onUserClickAddConnection",
    "ON_CANCEL": "onCancel"
}; 

// Search options
var SEARCH_OPTIONS = {
    DOCTOR: 'Doctor',
    OFFICE: 'Office',
    PORTAL: 'Portal'
};

module.exports.GA_DIMENSION = GA_DIMENSION;
module.exports.GA_EVENT = GA_EVENT;
module.exports.USER_EVENTS = USER_EVENTS;
module.exports.SEARCH_OPTIONS = SEARCH_OPTIONS;