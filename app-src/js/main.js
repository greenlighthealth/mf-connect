var _ = require('lodash');
var $ = require('jquery');
var mfConnectService = require('./mf-connect-service.js');
var mfUtils = require('./mf-utils.js');
var googleAnalytics = require('./google-analytics.js');
var prototype = require('./prototype.js');
var CONSTANTS = require('./constants');

(function (window) {

    // MfConnect constructor, doesn't do much now but should initialize variables maybe?
    // idk what should be in this but maybe something?
    function MfConnect() {
        this.api = mfConnectService;
    }

    // make mfConnectService api calls available through MfConnect.prototype.api for SDK use
    MfConnect.prototype.api = mfConnectService;

    var GA_EVENT = CONSTANTS.GA_EVENT;
    var USER_EVENTS = CONSTANTS.USER_EVENTS;

    MfConnect.prototype.launch = function (options) {
        MfConnect.prototype.api.setMfConnectData(options)
            .then(function (userData) {
                try {
                    googleAnalytics.setGAConfigs(options);
                } catch (error) {
                    console.log('error setting Google Analytics configurations');
                    console.log(error);
                }
                MfConnect.prototype.openModal();
            }, function (error) {
                console.log('error setting mfconnectdata');
                console.log(error);
            });
    };

    // close MfConnect modal - remove the entire modal and the overlay
    MfConnect.prototype.close = function (triggerEvent) {
        this.mfConnectWrapper.parentNode.removeChild(this.mfConnectWrapper);
        clearTimeoutAndInterval();
        clearConnectionDetailTimer();
        clearConnectionOverviewTimer();
        if (triggerEvent) {
            prototype.invokeEventHandler(USER_EVENTS.ON_CLOSE_DIALOG, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_CLOSE_DIALOG));
        }
    };

    MfConnect.prototype.invokeOnDoneMakingConnections = function () {
        prototype.invokeEventHandler(USER_EVENTS.ON_DONE_MAKING_CONNECTIONS, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_DONE_MAKING_CONNECTIONS));
        MfConnect.prototype.close(false);
    };

    // open MfConnect modal - build out modal, initialize close event
    MfConnect.prototype.openModal = function () {
        var startTime = googleAnalytics.startGASiteSpeedTimer();
        buildOutModal.call(this);

        if (prototype.hasPreSelectedPortal()) {
            showPreSelectedPortal(this.api.getMfConnectData()["preSelectedPortal"], 'createConnectionResults', null, true);
        } else {
            var successHandler = function (profileData) {
                // In MF-7998 we need to reorder and update screens because of this code is commented.
                // if (profileData.connectionsList.length === 0) {
                goToSearchForConnection('connectionOverview');
                // } else {
                //     createConnectionOverviewContent();
                // }
            };

            var errorHandler = function (error) {
                prototype.invokeOnErrorHandler(error);
            };

            mfUtils.findConnections().then(successHandler, errorHandler);
        }
        initializeEvents.call(this);
        this.connectModal.style.display = 'block';
        this.overlay.className = this.overlay.className + ' connect-open';

        prototype.invokeEventHandler(USER_EVENTS.ON_OPEN_DIALOG, googleAnalytics.generateGATrackingEventObj('Modal Event', USER_EVENTS.ON_OPEN_DIALOG), { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    };

    /*
     *  This function builds out the base/outline of the modal
     *  and creates the overlay
     *  It initializes the modal content with the connection overview screen
     */
    function buildOutModal() {
        this.mfConnectWrapper = createHtmlElement('div', 'mfConnectWrapper', 'mf-connect-wrapper');

        // add overlay
        this.overlay = createHtmlElement('div', undefined, 'mf-connect-overlay fade-and-drop');
        this.mfConnectWrapper.appendChild(this.overlay);

        // initial modal div
        this.connectModal = createHtmlElement('div', 'mfConnectModal', 'modal mf-modal');

        // modal dialog
        this.modalDialog = createHtmlElement('div', 'mfModalDialog', 'modal-dialog mf-modal-dialog');

        // modal content
        this.modalContent = createHtmlElement('div', 'mfModalContent', 'modal-content mf-modal-content');

        // modal loading overlay
        this.loadingOverlay = createHtmlElement('div', 'mfLoadingOverlay', 'mf-loading-overlay');
        this.loadingMask = createHtmlElement('div', 'mfLoadingMask', 'mf-loading-mask');
        this.spinner = createHtmlElement('span', 'mfSpinner', 'mf-icon mf-icon__loader');
        this.loadingOverlay.appendChild(this.loadingMask);
        this.loadingOverlay.appendChild(this.spinner);
        this.modalContent.appendChild(this.loadingOverlay);

        // modal header with close button
        this.modalHeader = createHtmlElement('div', 'mfModalHeader', 'modal-header mf-modal-header');

        this.buttonHolder = createHtmlElement('div', 'mfButtonHolder', undefined);

        this.modalBack = createHtmlElement('a', 'mfConnectBack', 'mf-back-button');
        this.buttonHolder.appendChild(this.modalBack);

        this.modalClose = createHtmlElement('button', 'mfCloseButton', 'close mf-close');
        this.modalClose.type = 'button';
        this.modalClose.innerHTML = '<span class="mf-icon mf-icon__x"></span>';

        this.buttonHolder.appendChild(this.modalClose);
        this.modalHeader.appendChild(this.buttonHolder);
        this.modalContent.appendChild(this.modalHeader);

        // scrollable container
        this.scrollableContainer = createHtmlElement('div', 'mfScrollable', 'mf-scrollable');

        // create modal body with connection overview
        // this will be the div that appends/removes children based on the view
        this.modalBody = createHtmlElement('div', 'mfCreateConnectionContent', 'modal-body mf-modal-body');

        // error section

        this.modalError = createHtmlElement('p', 'mfConnectError', 'mf-connect-error');

        this.modalError.style.display = 'none';
        this.modalBody.appendChild(this.modalError);
        this.scrollableContainer.appendChild(this.modalBody);

        // footer
        this.modalFooter = createHtmlElement('div', 'mfModalFooter', 'modal-footer mf-modal-footer');

        // branding
        this.brand = createHtmlElement('div', 'mfBrand', 'mf-brand');
        glLogo = document.createElement("img");
        glLogo.src = "https://greenlighthealth.com/wp-content/uploads/2020/01/GL-logo.horz_.white_.280x56.svg";
        glLogo.classList.add("greenlight-logo")
        this.brand.appendChild(glLogo);
        this.modalFooter.appendChild(this.brand);
        this.scrollableContainer.appendChild(this.modalFooter);

        this.modalContent.appendChild(this.scrollableContainer);
        this.modalDialog.appendChild(this.modalContent);
        this.connectModal.appendChild(this.modalDialog);

        // add modal to wrapper
        this.mfConnectWrapper.appendChild(this.connectModal);
        document.body.appendChild(this.mfConnectWrapper);
    }

    /*
     *  Initializes close events for the modal, on the close button and overlay click (overlay click might not work)
     */
    function initializeEvents() {
        if (this.modalClose) {
            this.modalClose.addEventListener('click', this.close.bind(this));
        }
        if (this.overlay) {
            this.overlay.addEventListener('click', this.close.bind(this));
        }
    }

    var SearchOptions = {
        DOCTOR: 'Doctor',
        OFFICE: 'Office',
        PORTAL: 'Portal'
    };

    // addAConnectionValues are things that we need to keep track of while the user
    // navigates through the 'Add a connection' flow. i.e. filter selection, headers and other constants specific to the path, etc.
    var addAConnectionValues = {};

    function setAddAConnectionValues(innerHTML) {
        if (innerHTML === 'Doctor name') {
            addAConnectionValues = {
                filter: SearchOptions.DOCTOR,
                searchSubHeader: 'Find your doctor',
                searchByLabel: 'Search by doctor name',
                searchByPlaceholderText: 'First and last name',
                selectSubHeader: 'Select your doctor'
            };
        } else if (innerHTML === 'Office name') {
            addAConnectionValues = {
                filter: SearchOptions.OFFICE,
                searchSubHeader: 'Find your doctor\'s location',
                searchByLabel: 'Search by office or hospital',
                searchByPlaceholderText: '',
                selectSubHeader: 'Select your doctor\'s location'
            };
        } else if (innerHTML === 'Portal website address') {
            addAConnectionValues = {
                filter: SearchOptions.PORTAL,
                searchSubHeader: 'Find your portal website',
                searchByLabel: 'Search by website address',
                searchByPlaceholderText: 'Example: careview.com/trianglehealth',
                selectSubHeader: 'Select your portal'
            };
        }
    }

    /*
     *  Display error to user
     *  Right now, the error field is at the top of the modal and we're just adding text to that field
     */
    function displayError(errorText) {
        changeErrorVisibility(true);
        document.getElementById('mfConnectError').innerHTML = errorText;
        prototype.invokeOnErrorHandler(errorText);
    }

    function createHtmlElement(name, id, className) {
        var elem = document.createElement(name);
        if (id !== undefined) {
            elem.id = id;
        }
        if (className !== undefined) {
            elem.className = className;
        }
        return elem;
    }

    function generateViewHeaders(headerTitle, subHeaderTitle, descriptionText, additionalDescription, classNames) {
        var headerContent = document.createElement('div');

        // header
        var header = document.createElement('h1');
        header.innerHTML = headerTitle;
        headerContent.appendChild(header);

        // sub header
        if (subHeaderTitle) {
            var subHeader = document.createElement('h2');
            subHeader.innerHTML = subHeaderTitle;
            headerContent.appendChild(subHeader);
        }

        //paragraph
        if (descriptionText) {
            var paragraph = document.createElement('p');
            paragraph.innerHTML = descriptionText;
            headerContent.appendChild(paragraph);
        }

        // another paragraph, if present
        if (additionalDescription) {
            var paragraph2 = document.createElement('p');
            paragraph2.innerHTML = additionalDescription;
            headerContent.appendChild(paragraph2);
        }

        // class names, if present
        if (classNames) {
            headerContent.classList.add(classNames);
        }

        return headerContent;
    }

    /*
     *  Display loading indicator and click block
     */
    function displayLoading(isLoading) {
        var loader = document.getElementById('mfLoadingOverlay');
        if (isLoading) {
            loader.style.display = 'block';
        } else {
            loader.style.display = 'none';
        }
    }

    function setNavigationBar(navigationName, onClickHandler) {
        var backButton = document.getElementById('mfConnectBack');

        if (backButton) {
            if (navigationName) {
                backButton.innerHTML = '<span class="mf-icon mf-icon__chevron-left--hollow--exact mf-color__action"></span>' + navigationName;
            }

            if (onClickHandler) {
                backButton.onclick = onClickHandler;
            }
        }

        toggleNavigation(true);
    }

    function toggleNavigation(isDisplayed) {
        var navigation = document.getElementById('mfConnectBack');

        if (navigation && isDisplayed) {
            navigation.style.display = 'inline-block';
        } else {
            navigation.style.display = 'none';
        }
    }

    function updateCurrentView(previousViewId, newViewElement) {
        if (previousViewId && newViewElement) {
            removePreviousView(previousViewId);
            document.getElementById('mfCreateConnectionContent').appendChild(newViewElement);
        }
    }

    function enrichInputElement(element, options) {
        if (element && options) {
            for (var optionProp in options) {
                if (options.hasOwnProperty(optionProp)) {
                    element[optionProp] = options[optionProp];
                }
            }
        }

        return element;
    }

    function generateInlineError(relatedInputId, validityState, customMessageMap) {
        var inlineError = createHtmlElement('label', 'mfErrorLabel__' + relatedInputId, 'mf-form__error mf-no-margin');
        inlineError.for = relatedInputId;

        var message = '';
        // this is map of error states from element validityState
        var defaultErrors = {
            VALUE_MISSING: {
                key: 'valueMissing',
                message: 'Please enter a value.'
            },
            TOO_LONG: {
                key: 'tooLong',
                message: 'Entered value is too long.'
            },
            TOO_SHORT: {
                key: 'tooShort',
                message: 'Entered value is too short.'
            },
            PATTERN_MISMATCH: {
                key: 'patternMismatch',
                message: 'Please enter value in correct format.'
            }
        };

        if (validityState) {
            for (var prop in defaultErrors) {
                if (defaultErrors.hasOwnProperty(prop)) {
                    var error = defaultErrors[prop];
                    if (validityState[error.key]) {
                        message = error.message;
                        if (customMessageMap[error.key]) {
                            message = customMessageMap[error.key];
                        }
                    }
                }
            }
        }

        inlineError.innerText = message;

        return inlineError;
    }

    function handleFormValidity(form) {
        var isFormValid = form.checkValidity();
        var formErrorClass = 'mf-error';

        if (isFormValid) {
            if (form.classList.contains(formErrorClass)) {
                form.classList.remove(formErrorClass);
            }
        } else {
            if (!form.classList.contains(formErrorClass)) {
                form.classList.add(formErrorClass);
            }
        }

        // we want to disable form button if the form itself is invalid
        var btn = form.getElementsByClassName('button')[0];
        btn.disabled = !isFormValid;
    }

    function validate(event, errorMessageMap) {
        var input = event.target;
        var inputId = input.id;
        var form = input.form;
        var invalidClass = 'invalid';

        // cleanup
        var inlineError = document.getElementById('mfErrorLabel__' + inputId);
        if (inlineError) {
            $(inlineError).remove();
        }

        if (input.validity.valid) {
            if (input.classList.contains(invalidClass)) {
                input.classList.remove(invalidClass);
            }
        } else {
            if (!input.classList.contains(invalidClass)) {
                input.classList.add(invalidClass);
            }

            inlineError = generateInlineError(inputId, input.validity, errorMessageMap);
            input.insertAdjacentElement('afterend', inlineError);
        }

        handleFormValidity(form);
    }

    function attachSearchByPortalUrlForm(parentDiv) {
        var inputId = 'primaryUrl';
        var inputLabel = getRequiredLabel(inputId, addAConnectionValues.searchByLabel);
        parentDiv.appendChild(inputLabel);

        var input = createHtmlElement('input', inputId, 'mf-form__input--text');
        var inputOptions = {
            type: 'search',
            required: true,
            placeholder: addAConnectionValues.searchByPlaceholderText,
            oninput: function (event) {
                var errorMessageMap = {
                    valueMissing: 'Please enter a portal website address.'
                };

                validate(event, errorMessageMap);
            }
        };

        parentDiv.appendChild(enrichInputElement(input, inputOptions));

        return parentDiv;
    }

    function attachDirectorySearchForm(parentDiv) {
        // search by doctor/office field
        var searchByInputId = 'searchBy';
        var searchByLabel = getRequiredLabel(searchByInputId, addAConnectionValues.searchByLabel);
        parentDiv.appendChild(searchByLabel);

        var searchByInput = createHtmlElement('input', searchByInputId, 'mf-form__input--text');
        var searchByInputOptions = {
            type: 'search',
            required: true,
            placeholder: addAConnectionValues.searchByPlaceholderText,
            minLength: 2,
            oninput: function (event) {
                var message = 'Please enter an office name.';
                if (addAConnectionValues.filter === SearchOptions.DOCTOR) {
                    message = 'Please enter a doctor name.';
                }
                var errorMessageMap = {
                    valueMissing: message,
                    tooShort: message
                };

                validate(event, errorMessageMap);
            }
        };

        parentDiv.appendChild(enrichInputElement(searchByInput, searchByInputOptions));

        // zip code field
        var zipCodeInputId = 'searchZip';
        var zipCodeLabel = getRequiredLabel(zipCodeInputId, 'Near zip code');
        parentDiv.appendChild(zipCodeLabel);

        var zipCodeInput = createHtmlElement('input', zipCodeInputId, 'mf-form__input--text');

        var errorMessageMap = {
            valueMissing: 'Please enter a valid zip code.',
            patternMismatch: 'Please enter a zip code in valid format.',
            tooShort: 'Please enter a valid zip code.'
        };

        var zipCodeInputOptions = {
            type: 'text',
            name: zipCodeInputId,
            required: true,
            pattern: '\\d*',
            minLength: 5,
            maxLength: 5,
            oninput: function (event) {
                // Don't validate on every keystroke, just check if the Submit button should be enabled
                handleFormValidity(parentDiv);

                // Clear any old error messages until the user focuses out again
                if (this.checkValidity()) {
                    validate(event, errorMessageMap);
                }
            },
            onblur: function (event) {
                // Only do proper validation on focusing out of the input
                validate(event, errorMessageMap);
            },
        };

        parentDiv.appendChild(enrichInputElement(zipCodeInput, zipCodeInputOptions));

        return parentDiv;
    }

    function getPortalSearchResults(previousView, previousSearchParams) {
        displayLoading(true);
        var searchInfo = {};
        if (!previousSearchParams) {
            searchInfo.primaryUrl = document.getElementById('primaryUrl').value;
        } else {
            // if user goes 'back' to this step, previousSearchParams will be passed in b/c 'primaryUrl' no
            // longer exist since we removed the parent node last time the user came through this step
            // we want to reinitialize searchInfo with just the primaryUrl. we don't care about anything else in
            // 'previousSearchParams' since from here moving forward the user will make a new selection
            searchInfo.primaryUrl = previousSearchParams.primaryUrl;
        }
        if (searchInfo.primaryUrl) {
            prototype.invokeOnSearchPortalHandler(searchInfo.primaryUrl);
            mfUtils.fetchPortalsByUrl(searchInfo.primaryUrl)
                .then(function (results) {
                    goToSearchResults(previousView, results, searchInfo);
                }, function (error) {
                    displayLoading(false);
                    displayError('Error getting portal search results.');
                });
        } else {
            displayLoading(false);
            displayError('Please enter a portal website address.');
        }
    }

    function showFindProviderView(previousView) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        var createConnectionSearch = createHtmlElement('div', 'createConnectionSearch', 'create-connection-search');

        // header
        var headerContent = generateViewHeaders('Add a portal', addAConnectionValues.searchSubHeader, null);
        // createConnectionSearch.appendChild(headerContent);

        // create form
        var searchForm = createHtmlElement('form', 'directorySearchForm', 'mf-form__group mf-no-margin');
        searchForm.name = 'directorySearchForm';
        searchForm.noValidate = true;       // turn off native browser validation

        if (addAConnectionValues.filter === SearchOptions.PORTAL) {
            searchForm = attachSearchByPortalUrlForm(searchForm);
        } else {
            searchForm = attachDirectorySearchForm(searchForm);
        }

        // create search button
        var searchBtn = createHtmlElement('input', 'directorySearchBtn', 'button mf-cta__primary--optional');
        searchBtn.type = 'submit';
        searchBtn.value = 'Search';
        searchBtn.disabled = true;
        searchBtn.onclick = function () {
            this.disabled = true;
            if (addAConnectionValues.filter === SearchOptions.PORTAL) {
                getPortalSearchResults('createConnectionSearch', undefined);
            } else {
                getDirectorySearchResults('createConnectionSearch', undefined, addAConnectionValues.filter);
            }
            return false;   // Need to return false here, otherwise the page reloads on submitting the form.
        };
        searchForm.appendChild(searchBtn);
        createConnectionSearch.appendChild(searchForm);

        if (addAConnectionValues.filter === SearchOptions.OFFICE || addAConnectionValues.filter === SearchOptions.DOCTOR) {
            var healthGradesBrand = createHtmlElement('p', undefined, 'mf-branding--subtle');
            healthGradesBrand.innerHTML = 'Directory data by Healthgrades';
            createConnectionSearch.appendChild(healthGradesBrand);
        }

        setNavigationBar('Back to select a search option', function () {
            goToSearchForConnection('createConnectionSearch');
        });
        displayLoading(false);
        // remove previous screen node
        // append new node we created here
        updateCurrentView(previousView, createConnectionSearch);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'Search_' + addAConnectionValues.filter }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function createFilterButtonId(id, innerHTML) {
        var classNm = '';
        if(id === 'mfPracticeBtn') {
            classNm = 'button mf-btn__portal mf-btn';
        } else {
            classNm = 'button mf-btn__connect mf-btn';
        }
        var button = createHtmlElement('button', id, classNm);
        button.type = 'button';
        button.innerHTML = innerHTML;
        return button;
    }

    function showFindById(previousView, recommendedPortals) {
        // reset addAConnectionValues whenever the user gets to this screen
        addAConnectionValues = {};

        var createFindById = createHtmlElement('div', 'create-find-by-id--container', 'create-find-by-id--container');

        //header
        var headerContent = generateViewHeaders('Connect Your Portals', null, null, null, 'create-find-by-id--header');
        createFindById.appendChild(headerContent);

        // Requested portals
        if (showRecommendedPortals(recommendedPortals)) {
            createFindById.appendChild(showRecommendedPortals(recommendedPortals));
            headerContent.classList.add('recommended-portals-displayed');
        }

        // select search option container
        var selectSearchOptionContainer = createHtmlElement('div', 'select-search-options--container', null);
        createFindById.appendChild(selectSearchOptionContainer);

        //description
        var selectSearchOptionDescription = document.createElement('p');
        // In MF-7994 we need to supprees search option because of this which code is commented.
        // selectSearchOptionDescription.innerHTML = 'Search for your doctor\'s patient portal using one of the following options.';
        selectSearchOptionContainer.appendChild(selectSearchOptionDescription);

        //filter buttons
        var mfPracticeBtn = createFilterButtonId('mfPracticeBtn', 'Office or hospital');
        mfPracticeBtn.onclick = function () {
            setAddAConnectionValues('Office name');
            showFindProviderView('create-find-by-id--container');
        };
        // In MF-7994 we need to supprees search option because of this which code is commented.
        // selectSearchOptionContainer.appendChild(mfPracticeBtn);

        var mfDoctorBtn = createFilterButtonId('mfDoctorBtn', 'Doctor name');
        mfDoctorBtn.onclick = function () {
            setAddAConnectionValues('Doctor name');
            showFindProviderView('create-find-by-id--container');
        };
        // In MF-7994 we need to supprees search option because of this which code is commented.
        // selectSearchOptionContainer.appendChild(mfDoctorBtn);

        var mfPortalBtn = createFilterButtonId('mfPortalBtn', 'Portal website address');
        mfPortalBtn.onclick = function () {
            setAddAConnectionValues('Portal website address');
            showFindProviderView('create-find-by-id--container');
        };
        // In MF-7994 we need to supprees search option because of this which code is commented.
        // selectSearchOptionContainer.appendChild(mfPortalBtn);

        // In MF-7998 we need to reorder and update screens because of this code is commented.
        // var profileData = mfConnectService.getMfConnectData().profileData;
        // if (profileData && profileData.connectionsList && profileData.connectionsList.length) {
        //   setNavigationBar('xyz', function () {
        //   createConnectionOverviewContent('create-find-by-id--container');
        //   });
        toggleNavigation(false);
        // } else {
        //     toggleNavigation(false);
        // }

        displayLoading(false);
        updateCurrentView(previousView, createFindById);
    }

    /*
         *  This builds out the content for the search form
         *  previousView is the id of the node from the previous view that we will remove from contentHolder
         */
    function goToSearchForConnection(previousView) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);
        displayLoading(true);

        var successHandler = function (recommendedPortals) {
            if (recommendedPortals.length === 0) {
                createConnectionOverviewContent(previousView);
            } else {
                showFindById(previousView, recommendedPortals);
            }
        };

        var errorHandler = function (error) {
            showRecommendedPortalsError();
        };
        mfUtils.findRecommendedPortalsListData().then(successHandler, errorHandler);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'Search_ChooseFilter' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    /*
     * checks for searchBy and searchZip, if nothing lets the user know that these are required fields
     * calls mfUtils getDirectorySearchResults which calls the api and returns results separated into practices and providers
     */
    // previousSearchParams is stuff we need to keep track of in order to come back from different views
    // since we are removing and recreating nodes every time we change
    function getDirectorySearchResults(previousView, previousSearchParams) {
        displayLoading(true);
        var searchInfo = {};
        if (!previousSearchParams) {
            searchInfo.searchTerm = document.getElementById('searchBy').value;
            searchInfo.zipCode = document.getElementById('searchZip').value;
        } else {
            // if user goes 'back' to this step, previousSearchParams will be passed in b/c 'searchBy' and 'searchZip' no
            // longer exist since we removed the parent node last time the user came through this step
            // we want to reinitialize searchInfo with just the searchTerm and zipCode. we don't care about anything else in
            // 'previousSearchParams' since from here moving forward the user will make a new selection
            searchInfo.searchTerm = previousSearchParams.searchTerm;
            searchInfo.zipCode = previousSearchParams.zipCode;
        }

        if (searchInfo.searchTerm && searchInfo.zipCode) {
            prototype.invokeOnSearchProviderHandler(searchInfo, addAConnectionValues.filter);
            mfUtils.findDirectoryLocations(searchInfo.searchTerm, searchInfo.zipCode, addAConnectionValues.filter)
                .then(function (results) {
                    goToSearchResults(previousView, results, searchInfo);
                }, function (error) {
                    displayLoading(false);
                    displayError(error.message);
                });
        } else {
            displayLoading(false);
            displayError('Please enter a search term and zip code.');
        }
    }

    function createSearchResultItem(name, address, clickFunction) {
        var practiceLi = createHtmlElement('li', undefined, 'mf-list__item mf-list--byline');
        practiceLi.innerHTML = '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
            '<p class="mf-list__element--primary">' + name + '</p>' +
            '<p class="mf-list__element--secondary">' + address + '</p>';
        practiceLi.onclick = clickFunction;
        return practiceLi;
    }

    function createPortalSearchResultItem(name, address, clickFunction) {
        var portalLi = createHtmlElement('li', undefined, 'mf-list__item mf-list__element-left mf-list--byline');
        portalLi.innerHTML = '<span class="mf-icon mf-icon__connections"></span>' +
            '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
            '<p class="mf-list__element--primary">' + name + '</p>' +
            '<p class="mf-list__element--secondary-wrap">' + address + '</p>';
        portalLi.onclick = clickFunction;
        return portalLi;
    }

    function createListHeader(resultCount, searchTerm) {
        var listHeader = createHtmlElement('h3', null, 'mf-list-header--small');
        listHeader.innerHTML = '<span>' + resultCount + ' Results: ' + searchTerm + '</span>';

        return listHeader;
    }

    function createPlacesListFromPractices(searchResults, createConnectionResults, searchInfo) {
        var placesHeader = createListHeader(searchResults.length, searchInfo.searchTerm);
        createConnectionResults.appendChild(placesHeader);

        var placesList = createHtmlElement('ul', null, 'mf-list--legacy mf-no-margin');

        _.forEach(searchResults, function (practice) {
            var name = mfUtils.getPracticeDisplayName(practice);
            var address = mfUtils.getPracticeDisplayAddress(practice);
            var practiceLi = createSearchResultItem(name, address, function () {
                directorySearchLocationClick(name, practice, searchInfo, 'createConnectionResults');
            });
            placesList.appendChild(practiceLi);
        });
        createConnectionResults.appendChild(placesList);
    }

    function createPeopleList(searchResults, createConnectionResults, searchInfo) {
        var providersHeader = createListHeader(searchResults.length, searchInfo.searchTerm);
        createConnectionResults.appendChild(providersHeader);

        var providersList = createHtmlElement('ul', null, 'mf-list--legacy mf-no-margin');

        _.forEach(searchResults, function (provider) {
            var displayName = mfUtils.getProviderDisplayName(provider);
            var displayAddress = mfUtils.getProviderDisplayAddress(provider);
            var providerLi = createSearchResultItem(displayName, displayAddress, function () {
                directorySearchProviderClick(provider, searchInfo, 'createConnectionResults');
            });
            providersList.appendChild(providerLi);
        });
        createConnectionResults.appendChild(providersList);
    }

    function createLocationList(locations, createConnectionResults, searchInfo) {
        var locationListHeader = createListHeader(locations.length, searchInfo.providerName);
        createConnectionResults.appendChild(locationListHeader);

        var locationList = createHtmlElement('ul', null, 'mf-list--legacy mf-no-margin');

        _.forEach(locations, function (location) {
            var name = mfUtils.getLocationName(location);
            var address = mfUtils.getFullLocationAddress(location.address);
            var practiceLi = createSearchResultItem(name, address, function () {
                directorySearchLocationClick(name, location, searchInfo, 'mfLocationsResults');
            });

            locationList.appendChild(practiceLi);
        });

        createConnectionResults.appendChild(locationList);
    }

    function createPortalList(searchResults, createConnectionResults, searchInfo) {
        var listLength = searchResults.portals.length;
        var portalsHeader = createListHeader(listLength, searchInfo.primaryUrl);
        createConnectionResults.appendChild(portalsHeader);

        var portalsList = createHtmlElement('ul', undefined, 'mf-list--legacy mf-no-margin');
        _.forEach(searchResults.portals, function (portal) {
            var displayName = portal.name;
            var portalUrl = portal.primaryUrl;
            var portalsLi = createPortalSearchResultItem(displayName, portalUrl, function () {
                portalSearchResultClick(portal, searchInfo);
            });
            portalsList.appendChild(portalsLi);
        });
        createConnectionResults.appendChild(portalsList);

        if (listLength === 0) {
            var noResultsForPortalSearch = document.createElement('p');
            noResultsForPortalSearch.innerHTML = 'Your search may have been too specific, try shortening the website address i.e. \'dukemychart.org\'';
            createConnectionResults.appendChild(noResultsForPortalSearch);
        }
    }

    /*
     * this builds out the content for the search results
     * removes previousView node and appends new node created
     * searchResults = results from the search split into practices and providers
     */
    function goToSearchResults(previousView, searchResults, searchInfo) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);

        var createConnectionResults = createHtmlElement('div', 'createConnectionResults', 'create-connection-results');

        // header
        var headerContent = generateViewHeaders('Add a portal', addAConnectionValues.selectSubHeader, null);
        createConnectionResults.appendChild(headerContent);
        // only display results if we actually have results to display and if the results match
        // the search criteria otherwise, tell the user no results
        if (addAConnectionValues.filter === SearchOptions.OFFICE && searchResults && searchResults.length) {
            createPlacesListFromPractices(searchResults, createConnectionResults, searchInfo);
        } else if (addAConnectionValues.filter === SearchOptions.DOCTOR && searchResults && searchResults.length) {
            createPeopleList(searchResults, createConnectionResults, searchInfo);
        } else if (addAConnectionValues.filter === SearchOptions.PORTAL && searchResults.portals) {
            createPortalList(searchResults, createConnectionResults, searchInfo);
        } else {
            // no results
            var noResults = document.createElement('p');
            noResults.innerHTML = 'No results found. Please search again';
            createConnectionResults.appendChild(noResults);
        }

        // set the back button
        setNavigationBar('Back to search', function () {
            showFindProviderView('createConnectionResults', null);
        });

        displayLoading(false);
        updateCurrentView(previousView, createConnectionResults);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'SearchResults_' + addAConnectionValues.filter }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    /*
     *  takes name of the search selection and directory object
     *  when user selects directory object, the next step is either:
     *      - select location if multiple locations
     *      - select portal if multiple portals
     *      - create connection step 2/enter credentials
     */
    function directorySearchProviderClick(provider, searchInfo, previousViewId) {
        displayLoading(true);
        var selectedProviderName = mfUtils.getProviderDisplayName(provider);
        prototype.invokeOnSelectProviderHandler(selectedProviderName, provider, SearchOptions.DOCTOR);

        searchInfo['selectedProvider'] = provider;

        mfUtils.getAllLocationsForProvider(provider.providerId).then(
            function (locations) {
                var locationCount = locations.length;

                if (locationCount) {
                    if (locationCount === 1) {
                        var preselectedLocation = locations[0];
                        var name = mfUtils.getLocationName(preselectedLocation);

                        directorySearchLocationClick(name, preselectedLocation, searchInfo, previousViewId);
                    } else {
                        var params = {
                            searchInfo: searchInfo
                        };

                        showMultipleLocationsView(previousViewId, locations, params);
                    }
                } else {
                    displayLoading(false);
                    displayError('Provider is not assigned to any location.');
                }
            },
            function (error) {
                displayLoading(false);
                displayError('Unable to load doctor\'s details.');
            }
        );
    }

    function directorySearchLocationClick(searchSelectionName, directoryObj, searchInfo, previousViewId) {
        displayLoading(true);
        prototype.invokeOnSelectProviderHandler(searchSelectionName, directoryObj, SearchOptions.OFFICE, searchInfo.originalFilter);

        searchInfo['selectedLocation'] = directoryObj;

        mfUtils.selectDirectoryObject(directoryObj).then(
            function (params) {
                params.directoryLocation.searchSelectionName = searchSelectionName;
                params.profileId = mfConnectService.getMfConnectData().profileId;
                params.searchInfo = searchInfo;

                if (params.nextStep === 'createConnectionSelectPortal') {
                    goToSelectPortal(previousViewId, params);
                } else if (params.nextStep === 'createConnectionEnterCredentials') {
                    goToEnterCredentials(previousViewId, params);
                } else {
                    displayLoading(false);
                    displayError('Error selecting directory object');
                }
            },
            function (error) {
                displayLoading(false);
                displayError('Error selecting directory object.');
            }
        );
    }

    function showMultipleLocationsView(previousState, locations, params) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        var currentViewId = 'mfLocationsResults';
        var locationResultsDiv = createHtmlElement('div', currentViewId, 'mf-locations-results');
        var selectedProviderName = params.searchInfo.selectedProvider.displayName;

        // the filter is going to change, so we want to keep track of where we got here from
        params.searchInfo['originalFilter'] = addAConnectionValues.filter;

        setAddAConnectionValues('Office name');
        params.searchInfo['providerName'] = selectedProviderName;

        // header
        var headerContent = generateViewHeaders('Add a connection', addAConnectionValues.selectSubHeader, null);
        locationResultsDiv.appendChild(headerContent);

        createLocationList(locations, locationResultsDiv, params.searchInfo);

        // set the back button
        setNavigationBar('Back to select doctor', function () {
            setAddAConnectionValues('Doctor name');
            getDirectorySearchResults(currentViewId, params.searchInfo);
        });

        displayLoading(false);
        updateCurrentView(previousState, locationResultsDiv);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'SearchResults_SelectLocation' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function portalSearchResultClick(portal, searchInfo) {
        var params = {
            portal: portal,
            profileId: mfConnectService.getMfConnectData().profileId,
            searchInfo: searchInfo
        };

        goToEnterCredentials('createConnectionResults', params);
    }

    /*
     *  builds out content for user to select portal if selected location has multiple portals
     */
    function goToSelectPortal(previousView, params) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);

        var currentViewId = 'createConnectionSelectPortal';
        var portals = params.portalArray;

        var createConnectionSelectPortal = createHtmlElement('div', currentViewId, 'create-connection-select-portal');

        var headerContent = generateViewHeaders('Add a portal', "Confirm your doctor's portal", "There are multiple patient portals associated with this doctor. Which one are you looking for?");
        createConnectionSelectPortal.appendChild(headerContent);

        var portalList = document.createElement('ul');
        portalList.className = 'mf-list--legacy mf-no-margin';

        _.forEach(portals, function (portal) {
            var portalLi = createHtmlElement('li', undefined, 'mf-list__item mf-list__element-left mf-list--byline');
            portalLi.innerHTML = '<span class="mf-icon mf-icon__connections"></span>' +
                '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
                '<p class="mf-list__element--primary">' + portal.name + '</p>' +
                '<p class="mf-list__element--secondary-wrap">' + portal.primaryUrl + '</p>';
            portalLi.onclick = function () {
                params['portal'] = portal;
                goToEnterCredentials(currentViewId, params);
            };
            portalList.appendChild(portalLi);
        });
        createConnectionSelectPortal.appendChild(portalList);

        setNavigationBar('Back to search results', function () {
            if (params.searchInfo.selectedProvider && params.searchInfo.selectedLocation) {
                params.searchInfo.selectedLocation = null;
                directorySearchProviderClick(params.searchInfo.selectedProvider, params.searchInfo, currentViewId);
            } else {
                getDirectorySearchResults(currentViewId, params.searchInfo);
            }
        });

        displayLoading(false);
        updateCurrentView(previousView, createConnectionSelectPortal);

        var loadTime = googleAnalytics.stopGASiteSpeedTimer(startTime)
        if (params.searchInfo.selectedProvider) {
            googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'SearchResults_Doctor_SelectPortal' }, { "loadTime": loadTime });
        } else {
            googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'SearchResults_Office_SelectPortal' }, { "loadTime": loadTime });
        }
    }

    function getRequiredLabel(relatedInputId, text) {
        var label = createHtmlElement('label', 'mfFormLabel__' + relatedInputId, 'mf-form__label');
        label.for = relatedInputId;
        label.innerHTML = text + '<span class="mf-form__label--required">Required</span>';

        return label;
    }


    function attachCredentialsForm(parentDiv, updateCredentialsForm) {
        var usernameId = "connectionCredentials_username";
        var passwordId = "connectionCredentials_password";
        if (updateCredentialsForm) {
            usernameId = 'update_username';
            passwordId = 'update_password';
        }

        // username field
        var usernameLabel = getRequiredLabel(usernameId, 'Portal username');
        parentDiv.appendChild(usernameLabel);

        var usernameInput = createHtmlElement('input', usernameId, 'mf-form__input--text');
        var usernameInputOptions = {
            required: true,
            autocomplete: 'off',
            oninput: function (event) {
                var errorMessageMap = {
                    valueMissing: 'Please enter a username.'
                };

                validate(event, errorMessageMap);
            }
        };
        parentDiv.appendChild(enrichInputElement(usernameInput, usernameInputOptions));

        // password field
        var passwordLabel = getRequiredLabel(passwordId, 'Portal password');
        parentDiv.appendChild(passwordLabel);

        var passwordInput = createHtmlElement('input', passwordId, 'mf-form__input--text mf-form__input--text-password');
        var passwordInputOptions = {
            type: 'password',
            required: true,
            autocomplete: 'new-password',
            oninput: function (event) {
                var errorMessageMap = {
                    valueMissing: 'Please enter a password.'
                };

                validate(event, errorMessageMap);
            }
        };
        parentDiv.appendChild(enrichInputElement(passwordInput, passwordInputOptions));

        return parentDiv;
    }

    function createPortalDescriptionHeader(viewParams, params) {
        // let the user know more information about the connection at this step
        var html = '';
        var name;
        if (viewParams.selectedPortal && viewParams.selectedPortal.name) {
            name = viewParams.selectedPortal.name;
        } else {
            name = params.directoryLocation.searchSelectionName;
        }
        // no existing connection (and we have a selectedPortal that is not IN_DEV and is not SUSPENDED)
        if (!mfUtils.hasSuccessfulExistingConnection(viewParams)) {
            html += "By providing your <span class='name-text name-text-bold'>" + name.toLowerCase() + "</span> username and password, you're enabling Greenlight to access and retrieve your health data.";
        } else if (viewParams.existingConnection) {
            html += "You have already connected to " + name + ". Please click 'Connect' to continue.";
        }
        // no selectedPortal or selectedPortal that is IN_DEV (placeholder portal)
        if (viewParams.selectedPortal && viewParams.selectedPortal.isUnderDevelopment()) {
            html += ' We\'re adding support for this provider\'s portal. We\'ll fetch your data once this portal is added.';
        } else if (viewParams.selectedPortal && viewParams.selectedPortal.isSuspended()) {
            html += ' This portal is experiencing connection issues. We\'ll fetch your data once that\'s fixed.';
        } else if (viewParams.selectedPortal && viewParams.selectedPortal.isInactive()) {
            html += ' This portal is no longer active. We will not be able to fetch your health data from it.';
        }
        return html;
    }

    function createPortalCredentialsView(divView, viewParams, params, previousView) {
        var currentViewId = 'createConnectionEnterCredentials';

        var headerContent;
        var portalDescription = createPortalDescriptionHeader(viewParams, params);
        if (params.portal) {
            headerContent = generateViewHeaders('Connect Your Portals', null);
            divView.appendChild(headerContent);
            var header = createHtmlElement('h3', 'recommended-portals--header', 'mf-list-header');
            header.innerHTML = portalDescription;
            divView.appendChild(header);
        } else {
            var pendingDescription = 'Please allow 24 hrs for our team to review if we support this patient portal.';
            headerContent = generateViewHeaders('Add a connection', 'Create a pending portal', pendingDescription, portalDescription);
            divView.appendChild(headerContent);
        }

        // create form
        var credentialsForm = createHtmlElement('form', 'enterCredentialsForm', 'mf-form__group mf-no-margin');
        credentialsForm.setAttribute('autocomplete', 'off');
        credentialsForm.name = 'enterCredentialsForm';

        var hasSuccessfulExistingConnection = mfUtils.hasSuccessfulExistingConnection(viewParams);

        if (!hasSuccessfulExistingConnection) {
            credentialsForm = attachCredentialsForm(credentialsForm, false);
        }

        // create connect button
        var createConnectionBtn = createHtmlElement('input', 'createConnectionBtn', 'button mf-cta__primary--optional');
        createConnectionBtn.type = 'submit';
        createConnectionBtn.value = 'Connect portal';
        createConnectionBtn.disabled = !hasSuccessfulExistingConnection;
        createConnectionBtn.onclick = function () {
            this.disabled = true;
            createNewConnection(currentViewId, params, viewParams);
            return false;
        };

        credentialsForm.appendChild(createConnectionBtn);

        divView.appendChild(credentialsForm);

        //create reset password link
        if (isPortalUrlAvailable(params.portal)) {
            var resetPasswordContainer = createHtmlElement('div', "resetPasswordContainer", 'create-connection-select-portal reset-psw-container');
            var resetPasswordLink = createHtmlElement('a', "", "pointer-cursor");
            resetPasswordLink.innerText = 'Forgot username or password?';
            resetPasswordLink.onclick = function () {
                showLeavingPageView(currentViewId, params);
            };
            resetPasswordContainer.appendChild(resetPasswordLink)
            divView.appendChild(resetPasswordContainer);
        }

        // set back button
        setNavigationBar('Back to search results', function () {
            if (params.portalArray) {
                goToSelectPortal(currentViewId, params);
            } else if (addAConnectionValues.filter === SearchOptions.PORTAL) {
                getPortalSearchResults(currentViewId, params.searchInfo);
            } else if (params.searchInfo.selectedProvider && params.searchInfo.selectedLocation) {
                if (addAConnectionValues.filter === SearchOptions.DOCTOR) {
                    // there was only one location available so it was pre-selected
                    // user needs to go back to select doctor
                    params.searchInfo.selectedLocation = null;
                    params.searchInfo.selectedProvider = null;
                    getDirectorySearchResults(currentViewId, params.searchInfo);
                } else {
                    params.searchInfo.selectedLocation = null;
                    directorySearchProviderClick(params.searchInfo.selectedProvider, params.searchInfo, currentViewId);
                }
            } else {
                getDirectorySearchResults(currentViewId, params.searchInfo);
            }
        });

        // set back button in case when we got to credentials page by clicking on recommended portal
        // or after modal is opened with pre-selected portal
        if (params.recommendedPortals || params.displayedOnOpen) {
            if (params.displayedOnOpen) {
                setNavigationBar('Skip', function () {
                    createConnectionOverviewContent(currentViewId);
                });
            } else {
                setNavigationBar('Back to search options', function () {
                    showFindById(currentViewId, params.recommendedPortals);
                });
            }
        }

        displayLoading(false);
        updateCurrentView(previousView, divView);
    }

    function isPortalUrlAvailable(portal) {
        var isUrlAvailable = false;

        if (portal && portal.isActive() && portal.primaryUrl) {
            isUrlAvailable = true;
        }

        return isUrlAvailable;
    }

    function showLeavingPageView(previousView, params) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        var portal = params.portal;
        var currentPageId = 'leavingPage';
        var leavingPage = createHtmlElement('div', currentPageId, 'mf-leaving-page mf-center-text');

        // image
        var connectionIcon = createHtmlElement('span', 'portalLauncherImg', 'mf-icon mf-icon__connection-large');
        leavingPage.appendChild(connectionIcon);

        // header
        var header = createHtmlElement('h2', 'leavingPageHeader', 'mf-header');
        header.innerHTML = 'Portal launcher';
        leavingPage.appendChild(header);

        var additionalInfo = createHtmlElement('p', 'additionalInfo', 'mf-additional-info mf-shrunk-text-center');
        additionalInfo.innerHTML = 'You\'re about to open <span class="mf-portal-name">' + portal.name + '</span> in a new window';
        leavingPage.appendChild(additionalInfo);

        // confirmation info
        var confirmationInfo = createHtmlElement('p', 'confirmationInfo', 'mf-confirmation-info mf-shrunk-text-left');
        confirmationInfo.innerHTML = '<div class="mf-confirmation-info-title">Instructions: </div> Launch your portal to reset or confirm your username and password, then return to complete adding this portal connection.';
        leavingPage.appendChild(confirmationInfo);

        var navigationButtons = createHtmlElement('div', 'navigationBtns', 'mf-navigation-btns');

        // cancel button
        var cancelButton = createHtmlElement('button', 'cancelBtn', 'mf-btn mf-naked mf-cancel-btn mf-cta__secondary');
        cancelButton.innerHTML = 'Cancel';
        cancelButton.onclick = function () {
            prototype.invokeOnCancel();
            goToEnterCredentials(currentPageId, params);
        };
        navigationButtons.appendChild(cancelButton);

        // launch button
        var launchButton = createHtmlElement('button', 'launchBtn', 'button mf-btn mf-launch-btn');
        launchButton.innerHTML = 'Launch portal';
        launchButton.onclick = function () {
            prototype.invokeOnRedirectHandler(portal.primaryUrl);
            window.open(portal.primaryUrl, '_blank');
            goToEnterCredentials(currentPageId, params);
        };
        navigationButtons.appendChild(launchButton);

        leavingPage.appendChild(navigationButtons);

        // set back button
        setNavigationBar('Back to add a portal', function () {
            goToEnterCredentials(currentPageId, params);
        });

        updateCurrentView(previousView, leavingPage);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'CreateConnection_LaunchPortal' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function goToEnterCredentials(previousView, params) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        var currentViewId = 'createConnectionEnterCredentials';
        // hide error when screen first loads
        changeErrorVisibility(false);
        displayLoading(true);

        if (previousView !== 'leavingPage') {
            var originalFilter = addAConnectionValues.filter;
            if (params.searchInfo && params.searchInfo.originalFilter) {
                originalFilter = params.searchInfo.originalFilter;
            }
            prototype.invokeOnSelectPortal(params.portal, originalFilter);
        }

        var divView = createHtmlElement('div', 'createConnectionEnterCredentials', 'create-connection-enter-credentials');
        var errorHandler = function (error) {
            // set back button
            // if coming from results
            setNavigationBar('Back to search results', function () {
                if (addAConnectionValues.filter === SearchOptions.PORTAL) {
                    getPortalSearchResults(currentViewId, params.searchInfo);
                } else if (params.searchInfo.selectedProvider && params.searchInfo.selectedLocation) {
                    params.searchInfo.selectedLocation = null;
                    directorySearchProviderClick(params.searchInfo.selectedProvider, params.searchInfo, currentViewId);
                } else {
                    getDirectorySearchResults(currentViewId, params.searchInfo);
                }

            });

            displayLoading(false);
            updateCurrentView(previousView, divView);
            displayError('Error loading data.');
        };
        var successHandler = function (viewParams) {
            createPortalCredentialsView(divView, viewParams, params, previousView);
        };
        mfUtils.initializeStep2Content(params).then(successHandler, errorHandler);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'CreateConnection_EnterCredentials' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    /*
     * create a new connection with the entered information
     */

    function createNewConnection(previousView, params, initialInfo) {
        //initial info has existing connection
        var connectionFields = {};
        var successfulExistingConnection = mfUtils.hasSuccessfulExistingConnection(initialInfo);

        if (!successfulExistingConnection) {
            connectionFields = {
                credentials: {
                    username: document.getElementById('connectionCredentials_username').value,
                    password: document.getElementById('connectionCredentials_password').value
                }
            };
        }

        if ((connectionFields.credentials && (!connectionFields.credentials.username || !connectionFields.credentials.password))) {
            displayError('Please enter a username and password.');
            // display error to user
        } else {
            displayLoading(true);
            mfUtils.createNewConnection(initialInfo, params)
                .then(function (connection) {
                    prototype.invokeOnCreateConnectionHandler(connection);
                    // In MF-7998 we need to reorder and update screens because of this code is commented.
                    // // portal is not active so go directly to overview
                    // if (!params.portal) {
                    //     createConnectionOverviewContent('createConnectionEnterCredentials');
                    // } else {
                    // goToValidatingCredentials(previousView, connection, params.portal.name);

                    mfUtils.findConnectionById(connection)
                        .then(function (extendedConnection) {
                            prototype.invokeValidatingCredentialsSuccessEvent(extendedConnection);
                            goToSearchForConnection('createConnectionEnterCredentials')
                        });
                    //  }
                }, function (error) {
                    var errorMsg = '';
                    if (error.status === 401) {
                        // This is the response code when we *know* the credentials are invalid --
                        // i.e., when they're connecting to a Medfusion portal.
                        errorMsg = 'The username and password you provided were not accepted by this doctor\'s patient portal. Please verify credentials and try again.';
                    } else {
                        errorMsg = 'There was an error creating a new connection. Please verify that your username and password are correct.';
                    }
                    displayLoading(false);
                    displayError(errorMsg);
                });
        }
    }

    var validatingCredentialsValues = {};

    // In MF-7998 we need to reorder and update screens because of this code is commented.
    // function goToValidatingCredentials(previousView, connection, portalName) {
    // In MF-7998 we need to reorder and update screens because of this code is commented.
    //var startTime = googleAnalytics.startGASiteSpeedTimer();

    //  changeErrorVisibility(false);
    //  displayLoading(true);

    // var currentView = 'validatingCredentials';

    // var validatingCredentials = createHtmlElement('div', 'validatingCredentials', 'validating-credentials mf-center-text');

    // var validatingIcon = createHtmlElement('span', null, 'mf-icon mf-icon__validating-credentials');
    // validatingCredentials.appendChild(validatingIcon);

    // var header = createHtmlElement('h2', null, null);
    // header.innerHTML = 'Validating username and password';
    // validatingCredentials.appendChild(header);

    // var description = createHtmlElement('p', null, null);
    // description.innerHTML = 'Make more portal connections while you wait for us to validate your username and password.';
    // validatingCredentials.appendChild(description);

    // var createConnectionBtn = createHtmlElement('button', null, 'button mf-cta__primary--optional');
    // createConnectionBtn.innerHTML = 'Add a portal';
    // createConnectionBtn.onclick = function () {
    //     prototype.invokeOnUserClickAddConnection();
    //     clearTimeoutAndInterval();
    //     goToSearchForConnection(currentView);
    // };
    // validatingCredentials.appendChild(createConnectionBtn);

    // var reviewConnectionsBtn = createHtmlElement('a', null, 'mf-link mf-center-text');
    // reviewConnectionsBtn.innerHTML = 'Review portal connections';
    // reviewConnectionsBtn.onclick = function () {
    //     prototype.invokeOnUserClickReviewConnections();
    //     clearTimeoutAndInterval();
    //     createConnectionOverviewContent(currentView);
    // };
    // validatingCredentials.appendChild(reviewConnectionsBtn);

    // if the credential check lasts 3 minutes, return to the connection management page
    // var timeout = window.setTimeout(function () {
    //     prototype.invokeValidatingCredentialsTimeoutEvent(connection);
    //     clearTimeoutAndInterval();
    //     createConnectionOverviewContent(currentView);
    // }, 180000);

    // check to see if the credentials were valid or invalid
    // var interval = window.setInterval(function () {
    // mfUtils.findConnectionById(connection)
    //     .then(function (extendedConnection) {
    //         prototype.invokeValidatingCredentialsSuccessEvent(extendedConnection);
    //         createConnectionOverviewContent(currentView);
    // if (extendedConnection.hasEverBeenSuccessful()) {
    //     prototype.invokeValidatingCredentialsSuccessEvent(extendedConnection);
    //    // clearTimeoutAndInterval();
    //     goToSuccessfullyConnected(currentView, extendedConnection, portalName);
    // } else if (!extendedConnection.hasEverBeenSuccessful() && !extendedConnection.isRetrieving() && extendedConnection.errorNeedsUserAuth()) {
    //     prototype.invokeValidatingCredentialsErrorEvent(extendedConnection);
    //    // clearTimeoutAndInterval();
    //     goToConnectionDetails(currentView, extendedConnection);
    // } else if (!extendedConnection.hasEverBeenSuccessful() && !extendedConnection.isRetrieving()) {
    //     prototype.invokeValidatingCredentialsErrorEvent(extendedConnection);
    //   //  clearTimeoutAndInterval();
    //     createConnectionOverviewContent(currentView);
    // }
    // });
    //}, 15000);

    // validatingCredentialsValues = {
    //   //  timeout: timeout,
    //     interval: interval
    // };

    // toggleNavigation(false);

    //   displayLoading(false);
    //  updateCurrentView(previousView, validatingCredentials);

    //   googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'CreateConnection_ValidatingCredentials' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    // }

    function clearTimeoutAndInterval() {
        if (validatingCredentialsValues && validatingCredentialsValues.timeout) {
            window.clearTimeout(validatingCredentialsValues.timeout);
        }
        if (validatingCredentialsValues && validatingCredentialsValues.interval) {
            window.clearInterval(validatingCredentialsValues.interval);
        }
    }

    function goToSuccessfullyConnected(previousView, connection, portalName) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        changeErrorVisibility(false);
        displayLoading(true);

        var successfullyConnected = createHtmlElement('div', 'successfullyConnected', 'successfully-connected mf-center-text');

        var successIcon = createHtmlElement('span', null, 'mf-icon mf-icon__connected-successful');
        successfullyConnected.appendChild(successIcon);

        var header = createHtmlElement('h2', null, null);
        header.innerHTML = 'Successfully connected';
        successfullyConnected.appendChild(header);

        var description = createHtmlElement('p', null, null);
        description.innerHTML = 'Thanks for waiting around! We are ready to add more portal connections.';
        successfullyConnected.appendChild(description);

        var newConnection = createHtmlElement('ul', null, 'mf-list--legacy mf-no-margin');

        var newConnectionItem = createHtmlElement('li', null, 'mf-list__item mf-list--byline');
        newConnectionItem.innerHTML = '<span class="mf-icon mf-icon__connected-small mf-list__pull-right"></span>' +
            '<p class="mf-list__element--primary">' + portalName + '</p>' +
            '<p class="mf-list__element--secondary">Connected</p>';

        newConnection.appendChild(newConnectionItem);
        successfullyConnected.appendChild(newConnection);

        var timeout = window.setTimeout(function () {
            clearTimeoutAndInterval();
            createConnectionOverviewContent('successfullyConnected');
        }, 4000);

        validatingCredentialsValues = {
            timeout: timeout,
        };

        displayLoading(false);
        updateCurrentView(previousView, successfullyConnected);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'CreateConnection_Success' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    var connectionStatusTimer = {};

    /*
     *  if provider needs an update, should show edit view with delete button,
     *  otherwise just show update and delete button
     */
    function goToConnectionDetails(previousView, connection) {
        mfUtils.findConnectionById(connection)
            .then(function (extendedConnection) {
                var startTime = googleAnalytics.startGASiteSpeedTimer();

                var metadata = { connectionId: extendedConnection.id, connectionStatus: extendedConnection.status, portalId: extendedConnection.portal.id };

                metadata.connectionId = metadata.connectionId.toString();
                metadata.portalId = metadata.portalId.toString();

                // hide error when screen first loads
                changeErrorVisibility(false);
                displayLoading(true);

                // possible status
                // isConnected: successfully linked and recieving data
                // errorNeedsUserAuth: we can't access this connection. Please verify and re-enter your login details.
                // errorNeedsUserInteraction: Please log in and accept the organization's Terms of Service.
                // hasUnknownError: Unable to sync your account data. Please check back shortly.
                // isRefreshing: we're currently checking the credentials of this connection.
                // isUnderDevelopment: We're adding support for this provider. We'll fetch your data once this provider is added.
                // isSuspended: This provider is experiencing connection issues. We'll fetch your data once that's fixed.
                // isPending: This is a pending portal. We'll fetch your data once this provider is added.

                var connectionDetails = createHtmlElement('div', 'connectionDetails', 'connection-details');

                var bannerStyle = mfUtils.getStatusStyleDetails(extendedConnection);

                var gradientStyle = '';
                if (bannerStyle && bannerStyle.gradientStyle) {
                    gradientStyle += bannerStyle.gradientStyle;
                }

                var bannerIcon = '';
                if (bannerStyle && bannerStyle.bannerIcon) {
                    bannerIcon += bannerStyle.bannerIcon;
                }

                var connectionDetailMessage = '';
                if (bannerStyle && bannerStyle.connectionDetailMessage) {
                    connectionDetailMessage += bannerStyle.connectionDetailMessage;
                }

                // add header
                var statusBannerHeader = createHtmlElement('ul', 'mfConnectionStatusHeader', 'mf-list--legacy mf-roofless mf-align-top mf-gradient ' + gradientStyle);

                var statusBannerItem = createHtmlElement('li', null, 'mf-list__item mf-borderless mf-list--byline mf-list__element-left');
                statusBannerItem.innerHTML = '<span class="mf-icon-header ' + bannerIcon + '"></span>' +
                    '<p class="mf-list__element--primary-header">Portal status</p>' +
                    '<p class="mf-list__element--secondary-header">' + connectionDetailMessage + '</p>';

                statusBannerHeader.appendChild(statusBannerItem);
                connectionDetails.appendChild(statusBannerHeader);

                if (extendedConnection.isPending()) {
                    var additionalInfo = createHtmlElement('div', null, 'mf-div-padding');
                    additionalInfo.innerHTML = '<p class="mf-single-line">Your provider </p><p class="mf-single-line semibold">' + extendedConnection.portal.name + '</p><p class="mf-single-line"> is under review to see if we support their portal type.</p>';
                    connectionDetails.appendChild(additionalInfo);
                }

                var header = createHtmlElement('p', null, 'no-margin-bottom small-text');
                header.innerHTML = 'MANAGE PORTAL CONNECTION';
                connectionDetails.appendChild(header);

                var portalNameHeader = createHtmlElement('h1', 'mfConnectionName', 'mf-connection-name semibold');
                portalNameHeader.innerHTML = extendedConnection.portal.name;
                connectionDetails.appendChild(portalNameHeader);

                // set the back button
                setNavigationBar('Back to portal connections', function () {
                    createConnectionOverviewContent('connectionDetails');
                });

                // begin button list
                var list = createHtmlElement('ul', null, 'mf-list--legacy mf-no-margin');

                // refresh connection
                var refreshConnectionItem = createHtmlElement('li', null, 'mf-list__item mf-list--byline mf-list__element-left');
                if (extendedConnection.isPortalActive() && (extendedConnection.isConnected() || extendedConnection.hasUnknownError() || extendedConnection.errorNeedsUserInteraction())) {
                    refreshConnectionItem.innerHTML = '<span class="mf-icon mf-icon__refresh"></span>' +
                        '<button id="mfRefreshButton" class="mf-btn mf-list__pull-right">Go</button>' +
                        '<p class="mf-list__element--primary">Refresh connection</p>' +
                        '<p class="mf-list__element--secondary">Download new records</p>';
                } else if (extendedConnection.isRetrieving()) {
                    refreshConnectionItem.innerHTML = '<span class="mf-icon mf-icon__refresh"></span>' +
                        '<button id="mfRefreshButton" class="mf-btn mf-list__pull-right mf-loading" disabled="disabled">Go</button>' +
                        '<p class="mf-list__element--primary">Refresh connection</p>' +
                        '<p class="mf-list__element--secondary">Download new records</p>';
                } else {
                    refreshConnectionItem.innerHTML = '<span class="mf-icon mf-icon__refresh mf-color__dim"></span>' +
                        '<button id="mfRefreshButton" class="mf-btn mf-list__pull-right" disabled="disabled">Go</button>' +
                        '<p class="mf-list__element--primary">Refresh connection</p>' +
                        '<p class="mf-list__element--secondary">Download new records</p>';
                }
                list.appendChild(refreshConnectionItem);

                // update connection
                var updateProviderBtn = null;
                if (extendedConnection.errorNeedsUserAuth()) {
                    updateProviderBtn = createHtmlElement('li', null, 'mf-list__item mf-list--byline mf-list__element-left');
                    updateProviderBtn.innerHTML = '<span class="mf-icon mf-icon__edit"></span>' +
                        '<span class="mf-icon mf-icon__invalid-small mf-list__pull-right"></span>' +
                        '<p class="mf-list__element--primary">Update sign in details</p>' +
                        '<p class="mf-list__element--secondary-negative">Update your information</p>';
                } else {
                    updateProviderBtn = createHtmlElement('li', null, 'mf-list__item mf-list__element-left');
                    updateProviderBtn.innerHTML = '<span class="mf-icon mf-icon__edit"></span>' +
                        '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
                        '<p class="mf-list__element--primary">Update sign in details</p>';
                }

                updateProviderBtn.onclick = function () {
                    clearConnectionDetailTimer();
                    goToUpdateCredentials('connectionDetails', extendedConnection);
                };
                list.appendChild(updateProviderBtn);

                // launch Portal
                if (extendedConnection.isPortalActive()) {
                    var launchPortalBtn = null;
                    if (extendedConnection.errorNeedsUserInteraction() || extendedConnection.errorNeedsSecurityQuestions()) {
                        launchPortalBtn = createHtmlElement('li', null, 'mf-list__item mf-list--byline mf-list__element-left');
                        launchPortalBtn.innerHTML = '<span class="mf-icon mf-icon__connections"></span>' +
                            '<span class="mf-icon mf-icon__invalid-small mf-list__pull-right"></span>' +
                            '<p class="mf-list__element--primary">Launch portal</p>' +
                            '<p class="mf-list__element--secondary-negative">' + bannerStyle.connectionManagementMessage + '</p>';
                    } else {
                        launchPortalBtn = createHtmlElement('li', null, 'mf-list__item mf-list__element-left');
                        launchPortalBtn.innerHTML = '<span class="mf-icon mf-icon__connections"></span>' +
                            '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
                            '<p class="mf-list__element--primary">Launch portal</p>';
                    }

                    launchPortalBtn.onclick = function () {
                        clearConnectionDetailTimer();
                        goToLaunchPortalView('connectionDetails', extendedConnection);
                    };

                    list.appendChild(launchPortalBtn);
                }

                // delete connection
                var deleteProviderBtn = createHtmlElement('li', null, 'mf-list__item mf-list__element-left');
                deleteProviderBtn.innerHTML = '<span class="mf-icon mf-icon__trash"></span>' +
                    '<span class="mf-icon mf-icon__chevron-right--hollow--exact--large mf-list__pull-right mf-color__dim"></span>' +
                    '<p class="mf-list__element--primary">Delete this connection</p>';
                deleteProviderBtn.onclick = function () {
                    clearConnectionDetailTimer();
                    goToDeleteConnection('connectionDetails', extendedConnection);
                };
                list.appendChild(deleteProviderBtn);
                connectionDetails.appendChild(list);

                displayLoading(false);
                updateCurrentView(previousView, connectionDetails);

                checkConnectionStatusTimer();

                // check connection status when coming from update sign in details page
                function checkConnectionStatusTimer() {
                    if (extendedConnection.isRetrieving()) {
                        var connectionStatusInterval = updateConnectionDetailsPage(extendedConnection, statusBannerItem);

                        connectionStatusTimer = {
                            connectionStatusInterval: connectionStatusInterval
                        };
                    } else {
                        clearConnectionDetailTimer();
                    }
                }

                //change header to refreshing connection
                document.getElementById('mfRefreshButton').addEventListener('click', function () {
                    if (document.getElementById('mfConnectionStatusHeader')) {
                        document.getElementById('mfConnectionStatusHeader').className = 'mf-list--legacy mf-roofless mf-align-top mf-gradient refreshing';
                    }

                    startConnectionRefreshingTimer();
                    refreshConnection(extendedConnection);

                    statusBannerItem.innerHTML = '<span class="mf-icon-header mf-icon__refreshing"></span>' +
                        '<p class="mf-list__element--primary-header">Connection status</p>' +
                        '<p class="mf-list__element--secondary-header">Looking for available data</p>';

                    statusBannerHeader.appendChild(statusBannerItem);

                    document.getElementById('mfRefreshButton').classList.add('mf-loading');
                    document.getElementById('mfRefreshButton').setAttribute('disabled', '');
                });

                // initialize and define start interval timer
                function startConnectionRefreshingTimer() {
                    var interval = updateConnectionDetailsPage(extendedConnection, statusBannerItem);

                    connectionStatusTimer = {
                        interval: interval
                    };
                }

                metadata.loadTime = googleAnalytics.stopGASiteSpeedTimer(startTime);

                googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'ConnectionDetails' }, metadata);
            });
    }

    function updateConnectionDetailsPage(connection, statusBannerItem) {
        return window.setInterval(function () {
            mfUtils.findConnectionById(connection)
                .then(function (extendedConnection) {
                    if (extendedConnection.status !== 'JOB_REFRESHING') {
                        var newStyle = mfUtils.getStatusStyleDetails(extendedConnection);

                        document.getElementById('mfConnectionStatusHeader').classList.replace('refreshing', newStyle.gradientStyle);

                        statusBannerItem.innerHTML = '<span class="mf-icon-header ' + newStyle.bannerIcon + '"></span>' +
                            '<p class="mf-list__element--primary-header">Connection status</p>' +
                            '<p class="mf-list__element--secondary-header">' + newStyle.connectionDetailMessage + '</p>';

                        document.getElementById('mfRefreshButton').classList.remove('mf-loading');

                        if (extendedConnection.isPortalActive() && (extendedConnection.isConnected() || extendedConnection.hasUnknownError() || extendedConnection.errorNeedsUserInteraction())) {
                            document.getElementById('mfRefreshButton').removeAttribute('disabled');
                        }

                        clearConnectionDetailTimer();
                    }
                });
        }, 30000);
    }

    function clearConnectionDetailTimer() {
        if (connectionStatusTimer && connectionStatusTimer.interval) {
            clearInterval(connectionStatusTimer.interval);
        } else if (connectionStatusTimer && connectionStatusTimer.connectionStatusInterval) {
            clearInterval(connectionStatusTimer.connectionStatusInterval);
        }
    }

    function refreshConnection(connection) {
        displayLoading(true);
        mfUtils.updateConnection(connection)
            .then(function (connection) {
                prototype.invokeConnectionRefreshEvent(connection);
                displayLoading(false);
            }, function (error) {
                displayLoading(false);
                displayError('Error updating connection.');
            });
    }

    function goToUpdateCredentials(previousView, connection) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);

        var updateCredentialsView = createHtmlElement('div', 'updateCredentialsView', 'update-credentials');

        // header
        var headerContent = document.createElement('div');

        var header = document.createElement('h1');
        header.innerHTML = 'Update sign in details';
        headerContent.appendChild(header);

        if (connection.errorNeedsUserAuth()) {
            var invalidInfoAlert = createHtmlElement('ul', null, 'mf-list--legacy mf-negative-alert');
            invalidInfoAlert.innerHTML = '<li class="mf-list__item mf-list__element-left">' +
                '<span class="mf-icon mf-icon__invalid-header"></span>' +
                '<p class="mf-list__element--primary-header-negative">Invalid information</p>' +
                '<p class="mf-list__element--secondary-header-negative">Please update your portal username and password</p></li>';
            headerContent.appendChild(invalidInfoAlert);
        }

        var subHeader = document.createElement('h2');
        subHeader.innerHTML = 'Updating your portal connection';
        headerContent.appendChild(subHeader);

        var paragraph = document.createElement('p');
        paragraph.innerHTML = 'Enter your username and password used when logging into ' + connection.portal.name;
        headerContent.appendChild(paragraph);

        updateCredentialsView.appendChild(headerContent);

        // update credentials form
        var updateLoginForm = createHtmlElement('form', 'updateLoginForm', 'mf-form__group mf-no-margin');
        updateLoginForm.setAttribute('autocomplete', 'off');
        updateLoginForm.name = 'updateLoginForm';

        updateLoginForm = attachCredentialsForm(updateLoginForm, true);

        // update credentials submit button
        var updateConnectionBtn = createHtmlElement('input', 'updateConnectionBtn', 'button mf-cta__primary');
        updateConnectionBtn.type = 'submit';
        updateConnectionBtn.value = 'Update';
        updateConnectionBtn.disabled = true;
        updateConnectionBtn.onclick = function () {
            this.disabled = true;
            updateConnection(connection);
            return false;
        };
        updateLoginForm.appendChild(updateConnectionBtn);
        updateCredentialsView.appendChild(updateLoginForm);

        // set the back button
        setNavigationBar('Back to manage connection', function () {
            goToConnectionDetails('updateCredentialsView', connection);
        });

        updateCurrentView(previousView, updateCredentialsView);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'ConnectionDetails_UpdateCredentials' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function updateConnection(connection) {
        var connectionFields = {
            credentials: {
                username: document.getElementById('update_username').value,
                password: document.getElementById('update_password').value
            }
        };

        if (!connectionFields.credentials.username || !connectionFields.credentials.password) {
            displayError('Please enter a username and password.');
            // display error to user
        } else {
            displayLoading(true);
            connection.credentials = connectionFields.credentials;
            mfUtils.updateConnection(connection)
                .then(function (connection) {
                    prototype.invokeConnectionUpdateEvent(connection);
                    displayLoading(false);
                    goToConnectionDetails('updateCredentialsView', connection);
                }, function (error) {
                    displayLoading(false);
                    displayError('Error updating connection.');
                });
        }
    }

    function goToLaunchPortalView(previousView, connection) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);

        var launchPortalView = createHtmlElement('div', 'launchPortalView', 'mf-leaving-page mf-center-text');

        // header
        var headerContent = document.createElement('div');

        var portalIcon = createHtmlElement('span', null, 'mf-icon mf-icon__connection-large');
        headerContent.appendChild(portalIcon);

        var header = document.createElement('h2');
        header.innerHTML = 'Log in to your portal to fix your account';
        headerContent.appendChild(header);

        launchPortalView.appendChild(headerContent);

        var selectedConnection = createHtmlElement('ul', null, 'mf-list--legacy mf-roofless inside-border');

        var selectedConnectionItem = createHtmlElement('li', null, 'mf-list__item mf-list--byline mf-borderless-center');
        selectedConnectionItem.innerHTML = '<p class="mf-list__element--primary">' + connection.portal.name + '</p>' +
            '<p class="mf-list__element--secondary-wrap mf-no-padding">' + connection.portal.primaryUrl + '</p>';

        selectedConnection.appendChild(selectedConnectionItem);
        launchPortalView.appendChild(selectedConnection);

        var buttonHeader = createHtmlElement('p', null, 'semibold');
        buttonHeader.innerHTML = 'Return and refresh connection';
        launchPortalView.appendChild(buttonHeader);

        var buttonBreak = createHtmlElement('ul', null, 'mf-list--legacy');
        launchPortalView.appendChild(buttonBreak);

        var navigationButtons = createHtmlElement('div', 'navigationBtns', 'mf-navigation-btns');

        var cancelBtn = createHtmlElement('button', null, 'mf-btn mf-naked mf-cancel-btn');
        cancelBtn.innerHTML = 'Cancel';
        cancelBtn.onclick = function () {
            prototype.invokeOnCancel();
            goToConnectionDetails('launchPortalView', connection);
        };
        navigationButtons.appendChild(cancelBtn);

        var launchPortalBtn = createHtmlElement('button', null, 'mf-btn');
        launchPortalBtn.innerHTML = 'Launch portal';
        launchPortalBtn.onclick = function () {
            prototype.invokeOnRedirectHandler(connection.portal.primaryUrl);
            window.open(connection.portal.primaryUrl, '_blank');
            goToConnectionDetails('launchPortalView', connection);
        };
        navigationButtons.appendChild(launchPortalBtn);

        launchPortalView.appendChild(navigationButtons);

        // set the back button
        setNavigationBar('Back to manage connection', function () {
            goToConnectionDetails('launchPortalView', connection);
        });

        updateCurrentView(previousView, launchPortalView);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'ConnectionDetails_LaunchPortal' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function goToDeleteConnection(previousView, connection) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // hide error when screen first loads
        changeErrorVisibility(false);

        var deleteConfirmationSection = createHtmlElement('div', 'deleteConfirmationSection', 'delete-connection mf-center-text mf-negative-alert-border');

        // header
        var headerContent = document.createElement('div');

        var deleteIcon = createHtmlElement('span', null, 'mf-icon mf-icon__trash-large mf-icon-large-margin');
        headerContent.appendChild(deleteIcon);

        var header = createHtmlElement('h1', null, 'negative');
        header.innerHTML = 'Are you sure you want to delete this connection?';
        headerContent.appendChild(header);

        deleteConfirmationSection.appendChild(headerContent);

        var selectedConnection = createHtmlElement('ul', null, 'mf-list--legacy mf-roofless inside-border');

        var selectedConnectionItem = createHtmlElement('li', null, 'mf-list__item mf-list--byline mf-border-negative');
        selectedConnectionItem.innerHTML = '<p class="mf-list__element--primary">' + connection.portal.name + '</p>' +
            '<p class="mf-list__element--secondary">' + connection.portal.primaryUrl + '</p>';

        selectedConnection.appendChild(selectedConnectionItem);
        deleteConfirmationSection.appendChild(selectedConnection);

        var confirmDelete = createHtmlElement('button', null, 'mf-btn mf-cta--danger');
        confirmDelete.innerHTML = 'Delete connection';
        confirmDelete.onclick = function () {
            deleteSelectedConnection(connection);
        };
        confirmDelete.style.display = 'inline-block';
        deleteConfirmationSection.appendChild(confirmDelete);

        var cancelDelete = createHtmlElement('button', null, 'mf-btn');
        cancelDelete.innerHTML = 'Cancel';
        cancelDelete.onclick = function () {
            prototype.invokeOnCancel();
            goToConnectionDetails('deleteConfirmationSection', connection);
        };
        cancelDelete.style.display = 'inline-block';
        deleteConfirmationSection.appendChild(cancelDelete);

        // set the back button
        setNavigationBar('Back to manage connection', function () {
            goToConnectionDetails('deleteConfirmationSection', connection);
        });

        updateCurrentView(previousView, deleteConfirmationSection);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'ConnectionDetails_Delete' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    /*
     *  delete connection and route back to connection overview
     */
    function deleteSelectedConnection(connection) {
        displayLoading(true);
        MfConnect.prototype.api.deleteConnection(connection.profile.id, connection.id)
            .then(function (response) {
                // go back to connection view wiht message
                prototype.invokeConnectionDeleteEvent(connection);
                createConnectionOverviewContent('deleteConfirmationSection');
            }, function (error) {
                displayLoading(false);
                // display error
                displayError('Error deleting provider.');
            });
    }

    function showRecommendedPortalsError() {
        displayLoading(false);
        displayError('Error loading requested portals.');
    }

    function createListOfProvidersErrorHandler(connectionOverview, error) {
        displayLoading(false);
        document.getElementById('mfCreateConnectionContent').appendChild(connectionOverview);
        displayError('Error loading connections.');
    }

    function createConnectionItem(connection) {
        var item = createHtmlElement('li', undefined, 'mf-list__item mf-list--byline');

        item.innerHTML = '';

        // show status icon based on connection status
        var styleDetails = mfUtils.getStatusStyleDetails(connection);

        if (styleDetails && styleDetails.managementPageIcon) {
            item.innerHTML += '<span class="mf-icon ' + styleDetails.managementPageIcon + ' mf-list__pull-right"></span>';
        }

        var connectionManagementMessage = '';
        if (styleDetails && styleDetails.connectionManagementMessage) {
            connectionManagementMessage += styleDetails.connectionManagementMessage;
        }

        var connectionManagementSecondaryStyle = '';
        if (styleDetails && styleDetails.connectionManagementSecondaryStyle) {
            connectionManagementSecondaryStyle += styleDetails.connectionManagementSecondaryStyle;
        }

        item.innerHTML += '<p class="mf-list__element--primary mf-list__item-color-brand">' + connection.portal.name + '</p><p class="' + connectionManagementSecondaryStyle + '">' + connectionManagementMessage + '</p>';

        item.onclick = function () {
            clearConnectionOverviewTimer();
            goToConnectionDetails('connectionOverview', connection);
        };
        return item;
    }

    function sortConnectionItemsById(connectionItems) {
        return connectionItems.sort(
            function (connectionItemOne, connectionItemTwo) {
                return connectionItemOne.id - connectionItemTwo.id;
            });
    }

    var connectionOverviewStatusTimer = {};

    function createListOfProviders(connectionOverview, profileData) {
        var connectionsListHeader = createHtmlElement('h3', null, 'mf-list-header');
        connectionsListHeader.innerHTML = 'Greenlight Health will attempt to validate your credentials so that we can access your health records. In the event that the username or password you provided were invalid. you will receive an email with a link to resolve it.';

        // create list of providers/connections
        var connectionsList = createHtmlElement('ul', 'connectionsList', 'mf-list--legacy mf-no-margin');

        if (profileData.connectionsList.length === 0) {
            var item = createHtmlElement('li', null, 'mf-list__item mf-list__element-left');
            item.innerHTML = '<span class="mf-icon mf-icon__add"></span><p class="mf-list__element--primary">Add a connection</p>';
            item.onclick = function () {
                goToSearchForConnection('connectionOverview');
            };
            connectionsList.appendChild(item);
        } else {
            _.forEach(sortConnectionItemsById(profileData.connectionsList), function (connection) {
                connectionsList.appendChild(createConnectionItem(connection));
            });

            var doneMakingConnectionsBtn = createHtmlElement('a', null, 'mf-link mf-center-text');
            doneMakingConnectionsBtn.innerHTML = 'Done adding portals';
            doneMakingConnectionsBtn.onclick = function () {
                MfConnect.prototype.invokeOnDoneMakingConnections();
            };

            connectionsList.appendChild(doneMakingConnectionsBtn);
        }

        function checkOverviewConnectionStatus() {
            var areConnectionsRefreshing;

            var interval = window.setInterval(function () {
                var connections = [];
                for (var i = 0; i < profileData.connectionsList.length; i++) {
                    if (profileData.connectionsList[i].status === 'JOB_REFRESHING') {
                        areConnectionsRefreshing = true;
                        connections.push(profileData.connectionsList[i]);
                    }
                }

                if (areConnectionsRefreshing) {
                    for (var i = 0; i < connections.length; i++) {
                        mfUtils.findConnectionById(connections[i])
                            .then(function (extendedConnection) {

                                if (extendedConnection.status !== 'JOB_REFRESHING') {
                                    var getconnectionListTag = document.getElementById('connectionsList');

                                    var portalNameTag = getconnectionListTag.getElementsByClassName('mf-list__element--primary');
                                    var portalNameArr = [].slice.call(portalNameTag);

                                    var subtitleTag = getconnectionListTag.getElementsByClassName('mf-list__element--secondary');
                                    var subtitleArr = [].slice.call(subtitleTag);

                                    var iconTag = getconnectionListTag.getElementsByClassName('mf-icon');

                                    for (var i = 0; i < portalNameArr.length; i++) {
                                        if (portalNameArr[i].innerText.includes(extendedConnection.portal.name)) {
                                            var newStyle = mfUtils.getStatusStyleDetails(extendedConnection);

                                            subtitleArr[i].innerHTML = '<p class="mf-list__element--secondary">' + newStyle.connectionDetailMessage + '</p>';
                                            iconTag[i].classList.replace('mf-icon__refreshing-small', newStyle.managementPageIcon);
                                        }
                                    }
                                }
                            });
                    }
                } else {
                    clearInterval(interval);
                }
            }, 60000);

            connectionOverviewStatusTimer = {
                interval: interval
            };
        }

        checkOverviewConnectionStatus();

        connectionOverview.appendChild(connectionsListHeader);
        connectionOverview.appendChild(connectionsList);

        // remove the back button
        toggleNavigation(false);

        displayLoading(false);
        document.getElementById('mfCreateConnectionContent').appendChild(connectionOverview);
    }

    function clearConnectionOverviewTimer() {
        if (connectionOverviewStatusTimer && connectionOverviewStatusTimer.interval) {
            clearInterval(connectionOverviewStatusTimer.interval);
        }
    }

    function showPreSelectedPortal(preSelectedPortal, previousView, recommendedPortals, onOpen) {
        displayLoading(true);

        var portalId = preSelectedPortal.portalId;
        var showCredentialsForm = function (portal) {
            var params = { "portal": portal, "profileId": mfConnectService.getProfileId() };
            if (recommendedPortals) {
                params['recommendedPortals'] = recommendedPortals;
            }

            if (onOpen) {
                params['displayedOnOpen'] = onOpen;
            }
            goToEnterCredentials(previousView, params);
        };

        var errorHandler = function (error) {
            if (error.status && error.status === 404) {
                createConnectionOverviewContent();
                return;
            }
            displayLoading(false);
            displayError('Error showing preselected portal');
        };

        var success = function (connections) {
            var isAlreadyConnected = connections.filter(function (connection) {
                return connection.portalId === JSON.stringify(portalId);
            }).length > 0;

            if (isAlreadyConnected) {
                createConnectionOverviewContent();
                return;
            }
            mfConnectService.findPortalById(portalId).then(showCredentialsForm, errorHandler);
        };
        mfConnectService.findAllConnections(mfConnectService.getProfileId()).then(success, errorHandler);
    }

    function showRecommendedPortals(recommendedPortals) {
        
        if (recommendedPortals.length === 0) {
            return;
        }

        var container = createHtmlElement('div', 'recommended-portals--container', null);

        var space = document.createElement('br');
        container.appendChild(space);

        var header = createHtmlElement('h3', 'recommended-portals--header', 'mf-list-header');
        header.innerHTML = 'Below are the portals associated with your doctors. We will walk you through providing your credentials to each of those portals so that Greenlight Health can obtain your health records.';
        container.appendChild(header);

        var list = createHtmlElement('ul', 'recommended-portals-list', 'mf-list--legacy mf-no-margin no-border');
        _.forEach(recommendedPortals, function (portal) {
            list.appendChild(createRecommendedPortalItem(portal, recommendedPortals));
        });
        container.appendChild(list);

        var portalTextContainer = createHtmlElement('div', 'recommended-portals--container', 'div-margin-top');
        var portalNoteHeader = createHtmlElement('h3', null, 'mf-list-header');
        // In MF-7994 we need to supprees search option because of this which code is commented.
        // portalNoteHeader.innerHTML = 'Don’t see your patient portal listed above or need to add another one?';
        portalTextContainer.appendChild(portalNoteHeader);
        container.appendChild(portalTextContainer);

        return container;
    }

    function createRecommendedPortalItem(recommendedPortal, recommendedPortals) {
        var portals = mfConnectService.getMfConnectData()["recommendedPortals"];
        if(portals) {
            portals.map(value => {
                var id = recommendedPortal.portal.id ? recommendedPortal.portal.id : recommendedPortal.portal.defaultLocation.portalId;
                if(value.portalId === id) {
                    recommendedPortal.portal.providedName = value.providedName ? value.providedName : null;
                }
            });
        }
        var mfPortalBtn = createFilterButtonId('mfPracticeBtn', null);
        var providedName = recommendedPortal.portal.providedName ? recommendedPortal.portal.providedName.toLowerCase() : null;
        var portalName = recommendedPortal.portal.name.toLowerCase();
        if(providedName) {
            mfPortalBtn.innerHTML = '<div><div><span class="name-text">' + providedName + '</span></div><div><span class="name-text">' + portalName + '</span></div></div><div><span class="small-fontsize">Click to enter your credentials</span></div>'
        } else {
            mfPortalBtn.innerHTML = '<div><span class="name-text">' + portalName + '</span></div><div><span class="small-fontsize">Click to enter your credentials</span></div>'
        }
        mfPortalBtn.onclick = function () {
            var portal = recommendedPortalToPreSelectedPortal(recommendedPortal);
            showPreSelectedPortal(portal, 'create-find-by-id--container', recommendedPortals);
        };
        return mfPortalBtn;
    }

    function recommendedPortalToPreSelectedPortal(recommendedPortal) {
        var portal = {};
        portal.portalId = recommendedPortal.portal.id;
        return portal;
    }

    /*
     *  build out connection overview content
     *  mfUtils initialize connection overview returns needed data for the view
     */
    function createConnectionOverviewContent(previousView) {
        var startTime = googleAnalytics.startGASiteSpeedTimer();

        // this view we want to hide the previous view first and display the loading indicator
        if (previousView) {
            removePreviousView(previousView);
        }

        if (previousView === 'connectionDetails') {
            clearConnectionDetailTimer();
        }

        // hide error when screen first loads
        changeErrorVisibility(false);
        displayLoading(true);

        var connectionOverview = createHtmlElement('div', 'connectionOverview', 'connection-overview');

        var headerContent = document.createElement('div');

        // header
        var header = document.createElement('h1');
        header.innerHTML = 'Connected portals';
        headerContent.appendChild(header);

        // add new connection btn
        var subHeader = document.createElement('h2');
        // var addNewConnection = createHtmlElement('button', undefined, 'mf-cta__primary--optional');
        // In MF-7998 we need to reorder and update screens because of this code is commented.
        // addNewConnection.innerHTML = 'Add a portal1';
        // addNewConnection.onclick = function () {
        //     clearConnectionDetailTimer();
        //     clearConnectionOverviewTimer();
        //     goToSearchForConnection('connectionOverview');
        // };
        //  subHeader.appendChild(addNewConnection);
        headerContent.appendChild(subHeader);

        // paragraph
        var addConnectionInfo = document.createElement('p');
        addConnectionInfo.innerHTML = 'What\'s next? The list below contains the portals that were requested for your records. If you think something is missing, please contact the requester.';
        headerContent.appendChild(addConnectionInfo);

        connectionOverview.appendChild(headerContent);

        var successHandler = function (profileData) {
            createListOfProviders(connectionOverview, profileData);
        };

        var errorHandler = function (error) {
            createListOfProvidersErrorHandler(connectionOverview, error);
        };
        mfUtils.findConnections().then(successHandler, errorHandler);

        googleAnalytics.trackGAnalyticsEvent(GA_EVENT.PAGEVIEW, { pageview: 'ConnectionsOverview' }, { "loadTime": googleAnalytics.stopGASiteSpeedTimer(startTime) });
    }

    function removePreviousView(previousView) {
        $('#' + previousView).remove();
    }

    function changeErrorVisibility(makeVisible) {
        var display = 'none';
        if (makeVisible) {
            display = 'block';
        }
        document.getElementById('mfConnectError').style.display = display;
    }

    window.MfConnect = MfConnect;
})(window);

(function () {
    'use strict';
    // this line is necessary for initializing MfConnect
    var mfConnect = new MfConnect();
})();